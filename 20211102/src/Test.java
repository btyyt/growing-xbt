//import java.util.Arrays;
//
///**
// * Created by YYT
// * Description:
// * User: YINYUNTAO
// * Date: 2021-11-02
// * Time: 18:20
// */
//
//public class Test {
////    public static void partition(int left, int right, int[] array) {
////        int i = left,j = right;
////        int tmp = array[left];
////        while (i < j) {
////            while (i < j && tmp <= array[j]) {
////                j--;
////            }
////            array[i] = array[j];
////            while (i < j && tmp >= array[i]) {
////                i++;
////            }
////            array[j] = array[i];
////        }
////        array[i] =tmp;
////
////    }
//    public static void quickSort(int left, int right ,int[] array) {//Hoare法
//        if(left > right) {
//            return;
//        }
//        int i = left,j = right;
//        int tmp = array[left];
//        while (i != j) {
//            while (i < j && tmp <= array[j]) {
//                j--;
//            }
//            while (i < j && tmp >= array[i]) {
//                i++;
//            }
//            if(i != j) {
//                int cent = array[i];
//                array[i] = array[j];
//                array[j] = cent;
//            }
//        }
//        array[left] = array[i];
//        array[i] = tmp;//标准值
//        quickSort(left,i-1,array);
//        quickSort(i+1,array.length-1,array);
//    }
//    public static void main(String[] args) {
//        int[] array = {2,4,1,7,5,8,9,10,3,6};
//        System.out.println(Arrays.toString(array));
//        partition(0,array.length-1,array);
//        //quickSort(0,array.length-1,array);
//        System.out.println(Arrays.toString(array));
//    }
//}
