import java.util.Arrays;

/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-11-02
 * Time: 21:18
 */
public class TestDemo2 {
    /**
     * 面试的时候，面试官现场让同学写的一个题
     * 将两个有序数组  合并成 一个有序数组
     * @param array1 [1,5,8,10]   m
     * @param array2 [3,4,9]      n
     * @return [1,3,4,5,8,9,10]
     * 时间复杂度：O(m+n)
     * 空间复杂度：O(m+n)
     */
    public static int[] mergeArray(int[] array1,int[] array2) {
        if(array1 == null) {
            return array2;
        }
        if(array2 == null) {
            return array1;
        }
        int[] tmp = new int[array1.length+array2.length];
        int k=0;
        int s1=0;
        int e1=array1.length-1;
        int s2=0;
        int e2=array2.length-1;
        while (s1 <= e1 && s2 <= e2) {
            if(array1[s1] <= array2[s2]) {
                tmp[k++] = array1[s1++];
//                k++;
//                s1++;
            } else {
                tmp[k++] = array2[s2++];
//                k++;
//                s2++;
            }
        }
        while (s1 <= e1) {
            tmp[k++] = array1[s1++];
        }
        while (s2 <= e2) {
            tmp[k++] = array2[s2++];
        }
        return tmp;
    }

    public static void main(String[] args) {
        int[] array1 = {1,5,7,10};
        int[] array2 = {3,6,9};
        int[] tmp = mergeArray(array1,array2);
        System.out.println(Arrays.toString(tmp));
    }
}
