/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-11-04
 * Time: 20:19
 */
class Vehicle {
    //功率、载人数和自重等，行为有启动、转向、关闭
    //public String name;
    public String power;
    public int capacity;
    public int weight;

    public Vehicle(String power, int capacity, int weight) {
        this.power = power;
        this.capacity = capacity;
        this.weight = weight;
    }

    public void startup() {
        System.out.println("正在启动");
    }
    public void sway() {
        System.out.println("正在向左向右转");
    }
    public void close() {
        System.out.println("正在关闭发动机");
    }
}
//汽车、飞机、轮船和火车
class Car extends Vehicle {
    public Car(String power,int capacity,int weight) {
        super(power,capacity,weight);
    }
    public String name;

    public Car(String power, int capacity, int weight, String name) {
        super(power, capacity, weight);
        this.name = name;
    }

    public void steponaccelerator() {//踩油门
        System.out.println(this.name+"正在踩油门");
    }

    @Override
    public String toString() {
        return "Car{" +
                "power='" + power + '\'' +
                ", capacity=" + capacity +
                ", weight=" + weight +
                ", name='" + name + '\'' +
                '}';
    }
}
class Aircraft extends Vehicle {
    public Aircraft(String power,int capacity,int weight) {
        super(power,capacity,weight);
    }
    public String name;

    public Aircraft(String power, int capacity, int weight, String name) {
        super(power, capacity, weight);
        this.name = name;
    }
    public void takeoff() {//起飞
        System.out.println(this.name+"正在起飞");
    }

    @Override
    public String toString() {
        return "Aircraft{" +
                "power='" + power + '\'' +
                ", capacity=" + capacity +
                ", weight=" + weight +
                ", name='" + name + '\'' +
                '}';
    }
}
class Ship extends Vehicle {
    public Ship(String power,int capacity,int weight) {
        super(power,capacity,weight);
    }
    public String name;

    public Ship(String power, int capacity, int weight, String name) {
        super(power, capacity, weight);
        this.name = name;
    }
    public void drift() {//漂流
        System.out.println(this.name+"正在漂流");
    }

    @Override
    public String toString() {
        return "Ship{" +
                "power='" + power + '\'' +
                ", capacity=" + capacity +
                ", weight=" + weight +
                ", name='" + name + '\'' +
                '}';
    }
}
class Train extends Vehicle{
    public Train(String power,int capacity,int weight) {
        super(power,capacity,weight);
    }
    public String name;

    public Train(String power, int capacity, int weight, String name) {
        super(power, capacity, weight);
        this.name = name;
    }
    public void Orbitchange() {//变轨
        System.out.println(this.name+"正在变轨");
    }

    @Override
    public String toString() {
        return "Train{" +
                "power='" + power + '\'' +
                ", capacity=" + capacity +
                ", weight=" + weight +
                ", name='" + name + '\'' +
                '}';
    }
}
public class Test {
    public static void main(String[] args) {
        Car car = new Car("100w",4,1000,"劳斯莱斯");
        System.out.println(car.toString());
        car.steponaccelerator();
        System.out.println("==============");
        Aircraft aircraft = new Aircraft("1000w",20,10000,"c919");
        System.out.println(aircraft.toString());
        aircraft.takeoff();
        System.out.println("==============");
        Ship ship = new Ship("1200w",50,20000,"泰坦尼克号");
        System.out.println(ship.toString());
        ship.drift();
        System.out.println("============= ");
        Train train = new Train("20000w",100,25000,"和谐号");
        System.out.println(train.toString());
        train.Orbitchange();
    }
}
