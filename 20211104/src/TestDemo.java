///**
// * Created by YYT
// * Description:
// * User: YINYUNTAO
// * Date: 2021-11-04
// * Time: 19:24
// */
//class People {
//    public String name;
//    public String sex;
//    public int age;
//    public int height;
//
//    public People(String name, String sex, int age, int height) {
//        this.name = name;
//        this.sex = sex;
//        this.age = age;
//        this.height = height;
//    }
//    public void eat(String food) {
//        System.out.println(this.name+"正在吃："+food);
//    }
//    public void shop(String goods) {
//        System.out.println(this.name+"正在购买："+goods);
//    }
//
//}
//class Teacher extends People {
//    public Teacher(String name, String sex, int age, int height) {
//        super(name,sex,age,height);
//    }
//    //职工号、职称、专业和工资
//    public int ad;
//    public String wookname;
//    public String major;
//    public int wages;
//
//    public Teacher(String name, String sex, int age, int height, int ad, String wookname, String major, int wages) {
//        super(name, sex, age, height);
//        this.ad = ad;
//        this.wookname = wookname;
//        this.major = major;
//        this.wages = wages;
//    }
//
//    public void Lecture() {
//        System.out.println(this.name + "正在上课");
//    }
//    public void Correct() {
//        System.out.println(this.name+ "正在批改作业");
//    }
//
//    @Override
//    public String toString() {
//        return "Teacher{" +
//                "name='" + name + '\'' +
//                ", sex='" + sex + '\'' +
//                ", age=" + age +
//                ", height=" + height +
//                ", ad=" + ad +
//                ", wookname='" + wookname + '\'' +
//                ", major='" + major + '\'' +
//                ", wages=" + wages +
//                '}';
//    }
//}
//class Student extends People {
//    public Student(String name, String sex, int age, int height) {
//        super(name,sex,age,height);
//    }
//    //学号、年级、专业和成绩
//    public int studentad;
//    public String grade;
//    public String smajor;
//    public int achievement;
//
//    public Student(String name, String sex, int age, int height, int studentad, String grade, String smajor, int achievement) {
//        super(name, sex, age, height);
//        this.studentad = studentad;
//        this.grade = grade;
//        this.smajor = smajor;
//        this.achievement = achievement;
//    }
//
//    public void Study() {
//        System.out.println(this.name + "正在学习课程");
//    }
//    public void Homework() {
//        System.out.println(this.name+ "正在做作业");
//    }
//
//    @Override
//    public String toString() {
//        return "Student{" +
//                "name='" + name + '\'' +
//                ", sex='" + sex + '\'' +
//                ", age=" + age +
//                ", height=" + height +
//                ", studentad=" + studentad +
//                ", grade='" + grade + '\'' +
//                ", major='" + smajor + '\'' +
//                ", achievement=" + achievement +
//                '}';
//    }
//}
//public class TestDemo {
//    public static void main(String[] args) {
//        Teacher teacher = new Teacher("马老师","男",38,170,001,"教室","武术",500);
//        System.out.println(teacher.toString());
//        teacher.eat("猪肉");
//        teacher.shop("牛肉");
//        teacher.Lecture();
//        teacher.Correct();
//        Student student = new Student("夏同学","男",20,165,2020,"大二","软件",88);
//        System.out.println(student.toString());
//        student.eat("青菜");
//        student.shop("手机");
//        student.Study();
//        student.Homework();
//    }
//}
