#include<stdio.h>//链式栈
#include<stdlib.h>
#define N 30
typedef int SLDataType;
typedef struct StackNode
{
	SLDataType data;
	struct StackNode *next;
}StackNode, *LinkStack;
int GetLenth(LinkStack s)
{
	StackNode *p = s;
	int cnt = 0;
	while (p)
	{
		cnt++;
		p = p->next;
	}
	return cnt;
}
int StackEmpty(LinkStack s)
{
	if (s = NULL)
		return 1;
	else
		return 0;
}
void PrintStack(LinkStack s)
{
	StackNode *p;
	p = s;
	while (p != NULL)
	{
		printf("%5d", p->data);
		p = p->next;
	}
	printf("\n");
}
int Push(LinkStack &s, int e)
{
	StackNode *p;
	p = (StackNode*)malloc(sizeof(StackNode));
	if (p != NULL)
	{
		p->data = e;
		p->next = s;
		s = p;
		return 1;
	}
	return 0;
}
int Pop(LinkStack &s, int &e)
{
	StackNode *p;
	if (s != NULL)
	{
		p = s;
		s = s->next;
		e = p->data;
		free(p);
		return 1;
	}
	else
	{
		return 0;
	}
}
int DestoryStack(LinkStack &s)
{
	StackNode *p;
	p = s;
	while (p != NULL)
	{
		s = s->next;
		free(p);
		p = s;
	}
	s = NULL;
	return 1;
}
int GetTop(LinkStack s, int &e)
{
	if (s!=NULL)
	{
		e = s->data;
		return 1;
	}
	else
	{
		return 0;
	}
}
int main()
{
	LinkStack s = NULL;
	int num, i;
	int value;
	printf("输入要入栈的个数：");
	scanf("%d", &num);
	i = 1;
	printf("输入要入栈的元素：");
	while (i <= num)
	{
		scanf("%d", &value);
		if (Push(s, value) == 0)
		{
			printf("%d入栈失败！", value);
			break;
		}
		i++;
	}
	printf("栈中的元素有：");
	PrintStack(s);
	if (Pop(s, value) == 0)
	{
		printf("栈顶元素成功出栈！\n");
		printf("出栈的元素为：%d\n", value);
		printf("栈顶元素出栈后，栈中的元素有：");
		PrintStack(s);
	}
	else
	{
		printf("栈顶元素出栈失败！\n");
	}
	if (GetTop(s, value) == 1)
	{
		printf("成功获取栈顶元素！\n");
		printf("栈顶的元素为：%d\n", value);
	}
	else
	{
		printf("获取栈顶元素失败！\n");
	}
	printf("栈中的元素有：");
	PrintStack(s);
	DestoryStack(s);
	return 0;
}

