Enter password: *******
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 2
Server version: 5.7.33-log MySQL Community Server (GPL)

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> create database test2;
Query OK, 1 row affected (0.00 sec)

mysql> use test2;
Database changed
mysql> create table test (name varchar(20);
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near '' at line 1
mysql> create table test (name varchar(20));
Query OK, 0 rows affected (0.02 sec)

mysql> insert into test values('汤老湿');
Query OK, 1 row affected (0.01 sec)

mysql> select * from test;
+-----------+
| name      |
+-----------+
| 汤老湿    |
+-----------+
1 row in set (0.00 sec)

mysql>