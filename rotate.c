#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
void rever(char *a, int k)
{
	int n = strlen(a);
	for (int i = 0; i < k; i++)
	{
		char str = *a;
		for (int j = 0; j < n - 1; j++)
		{
			*(a + j) = *(a + j + 1);
		}
		*(a + n - 1) = str;
	}
}
int main()
{
	char a[] = "ABCDEF";
	int k = 5;
	rever(a,k);
	printf("%s", a);
	return 0;
}