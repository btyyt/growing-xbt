/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-08-26
 * Time: 15:54
 */

import java.util.Arrays;
public class Test {
    public static void main(String[] args) {
        int[] arr={1,2,3,4,5,6};
        System.out.println(Arrays.toString(transform(arr)));

    }
    public static int[] transform(int[] arr){
        int left=0;
        int right=arr.length-1;
        while(left<right){
            //该循环结束，left指向一个奇数
            while(left<right&&arr[left]%2==0){
                left++;
            }
            //该循环结束，right指向一个偶数
            while(left<right&&arr[right]%2!=0){
                right--;
            }
            /// 交换两个元素的位置
            int temp=arr[left];
            arr[left]=arr[right];
            arr[right]=temp;
        }
        return arr;
    }
}

