import java.util.Scanner;

/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-11-05
 * Time: 17:06
 */
public class Test {
    public static void main(String[] args) {
        int[] array = {1,3,5};
        try {
            System.out.println("before");
            System.out.println(array[100]);
            System.out.println("after");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println("finally");
        }
        System.out.println("after try catch");
        try(Scanner sc = new Scanner(System.in)) {
            int num = sc.nextInt();
            System.out.println("num= "+num);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
