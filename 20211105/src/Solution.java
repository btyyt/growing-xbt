import java.util.*;

/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-11-05
 * Time: 8:54
 */
/**
 *    public class ListNode {
 *        int val;
 *        ListNode next = null;
 *
 *        ListNode(int val) {
 *            this.val = val;
 *        }
 *    }
 *
 */
import java.util.ArrayList;

public class Solution {
    public static String replaceSpace(StringBuffer str) {
        int length = str.length();
        char[] array = new char[length*3];
        int sum = 0;
        for(int i=0; i<length; i++) {
            char c = str.charAt(i);
            if(c == ' ') {
                array[sum++] = '%';
                array[sum++] = '2';
                array[sum++] = '0';
            } else {
                array[sum++] = c;
            }
        }
        String newStr = new String(array, 0, sum);
        return newStr;
    }

    public static void main(String[] args) {
        String str = "We Are Happy";
        StringBuffer s = new StringBuffer("We Are Happy");
        String newstr = replaceSpace(s);
        System.out.println(newstr.toString());
    }
}
