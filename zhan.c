#include<stdio.h>
#include<stdlib.h>
#define N 30
typedef int SLDataType;
typedef struct Stack
{
    SLDataType *base;//栈底元素的地址
    int top;//栈顶元素的位置
} Stack;
SLDataType initStack(Stack &S)
{
  S.base=(SLDataType*)malloc(N*sizeof(SLDataType));
  if(S.base==NULL)
        return -1;
  S.top=0;
  return 1;
}
SLDataType pushStack(Stack &S,int e)//输入栈的元素
{
    if(S.top==N)
        return 0;
    S.base[S.top]=e;
    S.top++;
    return 1;
}
void printStack(Stack &S)
{
    int i;
    i=0;
    while(i<S.top)
    {
        printf("%d ",S.base[i]);
        i++;
    }
    printf("\n");
}
int main()
{
    Stack S;
    int i,n,m;//n是入栈的个数
    if(initStack(S)==1)
        printf("栈初始化成功\n");
    printf("入栈的元素个数为：");
    scanf("%d",&n);
    i=1;
    printf("输入要入栈的元素：");
    while(i<=n)
    {
        scanf("%d",&m);
        if(pushStack(S,m)==0)
        {
            printf("%d入栈失败！\n",m);
            break;
        }
        i++;
    }
    printf("栈中的元素有：    ");
    printStack(S);//打印栈中的元素
    return 0;
}
