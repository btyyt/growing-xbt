import java.util.*;
/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-10-31
 * Time: 13:05
 */
public class TestDemo {
    public static void selectSort(int[] a) {//不稳定的排序
        if(a == null) {
            return;
        }
        for(int i=0; i<a.length; i++) {
            int max = 0;
            for(int j=i+1; j<a.length; j++) {
                if(a[i] > a[j]) {
                    int tmp = a[i];
                    a[i] = a[j];
                    a[j] = tmp;
                }
            }
        }
    }
    public static void shell(int[] a,int gap) {
        if(a == null) {
            return ;
        }
        for(int i=gap; i<a.length; i++) {
            int tmp = a[i];
            int j = i-gap;
            for( ; j >=0; j-=gap) {
                if(a[j] > tmp) {//a[j+gap] > tmp
                    a[j+gap] = a[j];
                } else {
                    break;
                }
            }
            a[j+gap] = tmp;
        }
    }
    public static void shellSort(int[] a) {
        int gap = a.length-1;
        while (gap > 1) {
            shell(a,gap);
            gap = gap/2;
            //insertSort(a,gap);
        }
        shell(a,1);
        //insertSort(a,1);
    }
    public static void insertSort(int[] a,int gap) {
        //参数判断
        if(a == null) {
            return ;
        }
        for (int i=1; i< a.length; i++) {
            int tmp = a[i];
            int j = 0;
            for (j=i-gap; j>=0; j--) {
                if(a[j] > tmp) {
                    a[j+gap] = a[j];
                } else {
                    break;
                }
            }
            a[j+gap] = tmp;
        }
    }

    public static void main(String[] args) {
        int[] a = {6,1,3,2,4,9,5,18,12,8};
        System.out.println(Arrays.toString(a));
        selectSort(a);
        System.out.println(Arrays.toString(a));
    }

    public static void main3(String[] args) {
        int[] a = {6,1,3,2,4,9,5,18,12,8};
        System.out.println(Arrays.toString(a));
        shellSort(a);
        System.out.println(Arrays.toString(a));
    }
    public static void main2(String[] args) {
        int[] a = {6,1,3,2,4};
        System.out.println(Arrays.toString(a));
        //insertSort(a);
        System.out.println(Arrays.toString(a));
    }
    public static void main1(String[] args) {//直接插入排序
        int[] a = {6,1,3,2,4};
        for (int s=0; s<a.length; s++) {
            System.out.print(a[s]+" ");
        }
        System.out.println();
        for(int i=1; i<a.length; i++) {
            int v = a[i];
            int j=i-1;
            for ( ; j>=0&&v<a[j]; j--) {
                a[j+1] = a[j];
            }
            a[j+1] = v;
        }
       for (int s=0; s<a.length; s++) {
           System.out.print(a[s]+" ");
       }
    }
}
