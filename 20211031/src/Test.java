/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-10-31
 * Time: 20:07
 */
class Base{

    public int getX() {

        return 1;

    }

    public static int getY() {

        return 2;

    }

}

class Sub extends Base{

    public int getX() {

        return 3;

    }

    public static int getY() {

        return 4;

    }

}

public class Test {

    public static void main(String[] args) {

        Sub sub=new Sub();

        Base base=(Base) sub;

        int a=sub.getX()*base.getX();

        int b=sub.getY()+base.getY();

        System.out.println(a+b);

    }

}


//class  X{
//
//    String name="Tom";
//
//    static void show(){
//
//        System.out.println("100");
//
//    }
//
//    void display(){
//
//        System.out.println("200");
//
//    }
//
//}
//
//class Y extends X{
//
//    String name="Jerry";
//
//    static void show(){
//
//        System.out.println("300");
//
//    }
//
//    void display(){
//
//        System.out.println("400");
//
//    }
//
//}
//
//public class Test {
//
//    public static void main(String[] args) {
//
//        X  x=new Y();
//
//        System.out.println(x.name);
//
//        x.show();
//
//        x.display();
//
//    }
//
//}
