#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int main()
{
	int i = 1;
	printf("%d\n", i += 1);
	printf("%d\n", i -= 1);
	printf("%d\n", i *= 2);
	printf("%d\n", i /= 2);
	printf("%d\n", i %= 2);
	printf("%d\n", i <<= 1);
	printf("%d\n", i >>= 1);
	printf("%d\n", i &= -1);
	printf("%d\n", i |= 1);
	printf("%d\n", i ^= 1);
	return 0;
}

