import java.util.Queue;
import java.util.Stack;

/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-10-18
 * Time: 17:11
 */
public class TestDemo {
    public static void main(String[] args) {
        Queue<Integer> queue = new Queue<>() ;

    }
    public static void main1(String[] args) {
        Stack<Integer> stack = new Stack<>();
        stack.push(10);
        stack.push(20);
        stack.push(30);
        System.out.println(stack);
        System.out.println(stack.peek());//获取栈顶元素
        System.out.println(stack);
        System.out.println(stack.pop());//弹出栈顶元素
        System.out.println(stack);
        System.out.println(stack.isEmpty());//判断栈是否为空
        System.out.println(stack.size());//计算栈的元素个数
        stack.push(50);
        System.out.println(stack.size());
    }
}
