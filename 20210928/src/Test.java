/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-09-28
 * Time: 19:33
 */

class Person {
    public int id;
    public String name;
    public int grade;
}
//public class Test {
//    public static void main(String[] args) {
//        boolean a;
//        System.out.println(a);
//    }
//}
//class Person {
//    String name;
//    void reading(String book) {
//        System.out.println(name+"正在阅读"+book);
//    }
//    void display() {
//        System.out.println("姓名："+name);
//    }
//}
//class Rectangle {
//    double length;
//    double width;
//    public double area() {
//        return length*width;
//    }
//    public double perimeter() {
//        return 2*(length+width);
//    }
//    public String getRectangle() {
//        return "长方形的长是"+length+",宽是："+width;
//    }
//}
//public class Test {
//    public static void main(String[] args) {
//        Rectangle r = new Rectangle();
//        r.length=10;
//        r.width=20.5;
//        System.out.println(r.getRectangle());
//        System.out.println("面积是："+r.area());
//        System.out.println("周长是："+r.perimeter());
//    }
//}
//public class Test {
//    public static void main(String[] args) {
//        Person me = new Person();
////        me.name="孔子";
////        me.reading("论语");
////        me.display();
//    }
//
//}
//class Person {
//
//    private String name;//实例成员变量
//    private int age;
//    private String sex;
//    //默认构造函数 构造对象
//
//    public void show(){
//        System.out.println("name: "+name+" age: "+age+" sex: "+sex);
//    }
//
//}
//public class Test{
//    public static void main(String[] args) {
//        Person p1 = new Person();//调用不带参数的构造函数 如果程序没有提供会调用不带参数的构造函数
//        p1.show();
//
//    }
//}

//public class Test {
//    public static void main(String[] args) {
//
//    }
//}

//class TestDemo {
//    private String name = "小明";
//    public void setName(String name) {
//        this.name=name;
//    }
//    public String getName() {
//        return name;
//    }
//    public void show() {
//        System.out.println("我是"+name);
//    }
//}
//public class Test {
//    public static void main(String[] args) {
//        TestDemo t = new TestDemo();
//        t.setName("xiaoming");
//        String name = t.getName();
//        System.out.println(name);
//        t.show();
//    }
//}
// class  TestDemo {
//     public static int count;
//     public static void change() {
//         count = 100;
//     }
//     public static void testchnage() {
//         count = 101;
//     }
//}
//public class Test {
//    public static void main(String[] args) {
//        TestDemo.change();
//        System.out.println(TestDemo.count);
//        TestDemo.testchnage();
//        System.out.println(TestDemo.count);
//    }
//}
//class Person {
//    public int a;
//    public static int count;
//    //a与count初始值都为0
//}
//public class Test {
//    public static void main(String[] args) {
//        Person person = new Person();
//        person.a++;
//        person.count++;
//        System.out.println(person.a);//1
//        System.out.println(person.count);//1
//        Person person1 = new Person();
//        person1.a++;
//        person.count++;
//        System.out.println(person1.a);//1
//        System.out.println(person1.count);//2
//    }
//}

//class Person {
//    public String name = "小明";
//    public int age = 18;
//}
//public class Test {
//    public static void main(String[] args) {
//        Person person = new Person();
//        System.out.println(person.name);
//        System.out.println(person.age);
//    }
//}
//class Person {
//    public String name;
//    public int age;
//}
//public class Test {
//    public static void main(String[] args) {
//        Person person = new Person();
//        System.out.println(person.name.length());
//    }
//}
