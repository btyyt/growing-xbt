#define _CRT_SECURE_NO_WARNINGS 1//用warshall 算法编程实现传递闭包运算，分组实现，题目自拟（四阶矩阵以上，原始关系矩阵为1的元素不少于3个）。
#include <stdio.h>
#define N 5     
int get_matrix(int a[N][N])
{
	int i = 0, j = 0;
	for (i = 0; i < N; i++)
	{
		for (j = 0; j < N; j++)
		{
			scanf("%d", &a[i][j]);
			if (a[i][j] != 0 && a[i][j] != 1)
				return 1;
		}
	}
	return 0;
}
int warshall(int a[][N])
{
	//(1)i＝1；
	//(2)对所有j如果a[j，i]＝1，则对k＝0，1，…，n-1，a[j，k]＝a[j，k]∨a[i，k]；
	//(3)i加1；
	//(4)如果i<n，则转到步骤2，否则停止
	int i = 0;
	int j = 0;
	int k = 0;
	for (i = 0; i < N; i++)
	{
		for (j = 0; j < N; j++)
		{
			if (a[j][i])
			{
				for (k = 0; k < N; k++)
				{
					a[j][k] = a[j][k] | a[i][k];//进行逻辑加 
				}
			}
		}
	}
}
int print_matrix(int a[N][N])
{
	int i = 0, j = 0;
	for (i = 0; i < N; i++)
	{
		for (j = 0; j < N; j++) 
		{
			printf("%d ", a[i][j]);
		}
		putchar('\n');
	}
}
int main()
{
	int a[N][N] = { 0 };
	printf("请输入初始矩阵 %d * %d:\n", N, N);
	if (get_matrix(a))
	{
		printf("获取失败，矩阵只要0或者1\n");
		return 1;  
	}
	warshall(a);
	printf("传递闭包运算后的矩阵为：\n");
	print_matrix(a);
	return 0; 
}

