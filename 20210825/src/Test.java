/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-08-25
 * Time: 9:51
 */

import java.util.Arrays;
public class Test {
    public static void main(String[] args) {
        int[] arr={1,2,3,4,5,6};
        System.out.println(Arrays.toString(transform(arr)));

    }
    public static int[] transform(int[] a){
        int left=0;
        int right=a.length-1;
        while(left<right){
            //该循环结束，left指向一个奇数
            while(left<right&&a[left]%2==0){
                left++;
            }
            //该循环结束，right指向一个偶数
            while(left<right&&a[right]%2!=0){
                right--;
            }
            /// 交换两个元素的位置
            int temp=a[left];
            a[left]=a[right];
            a[right]=temp;
        }
        return a;
    }
}


//public class Test {
//    public static void print(int n,int m,int[] arr1,int[] arr2) {
//        for(int i=0;i<n;i++) {
//            System.out.println(arr1[i]);
//        }
//        for(int j=0;j<arr2.length;j++) {
//            System.out.println(arr2[j]);
//        }
//    }
//    public static void swap(int[] arr1,int[] arr2) {
//        for(int i=0;i<arr1.length&&i<arr2.length;i++) {
//            int tmp=arr1[i];
//            arr1[i]=arr2[i];
//            arr2[i]=tmp;
//        }
//    }
//    public static void main(String[] args) {
//        int[] arr1={1,2,3};
//        int[] arr2={4,5,6};
//        print(arr1.length,arr2.length,arr1,arr2);
//        System.out.println("=======");
//        swap(arr1,arr2);
//        print(arr1.length,arr2.length,arr1,arr2);
//    }
//}

//class Calculator {
//    private int num;
//
//    public int getNum() {
//        return num;
//    }
//
//    public void setNum(int num) {
//        this.num = num;
//    }
    //    private int num1;
//    private int num2;
//
//    public int getNum1() {
//        return num1;
//    }
//
//    public void setNum1(int num1) {
//        this.num1 = num1;
//    }
//
//    public int getNum2() {
//        return num2;
//    }
//
//    public void setNum2(int num2) {
//        this.num2 = num2;
//    }
//
//    public int add() {
//        return this.num1+this.num2;
//    }
//
//    public int sub() {
//        return this.num1-this.num2;
//    }
//
//    public int mul() {
//        return this.num1*this.num2;
//    }
//
//    public double dev() {
//        return this.num1*1.0/this.num2;
//    }
//}
//public class Test {
//    public static void swap(Calculator calculator,  Calculator calculator1) {
//        int tmp=calculator.getNum();
//        calculator.setNum(calculator1.getNum());
//        calculator1.setNum(tmp);
//    }
//    public static void main(String[] args) {
//        Calculator calculator = new Calculator();
//        calculator.setNum(10);
//        Calculator calculator1  =new Calculator();
//        calculator1.setNum(20);
//
//        System.out.println(calculator.getNum());
//        System.out.println(calculator1.getNum());
//        swap(calculator,calculator1);
//        System.out.println("=======");
//
//        System.out.println(calculator.getNum());
//        System.out.println(calculator1.getNum());
//    }
//    public static void main(String[] args) {
//        Calculator calculator =new Calculator();
//        calculator.setNum1(10);
//        calculator.setNum2(20);
//
//        System.out.println(calculator.add());
//        System.out.println(calculator.sub());
//        System.out.println(calculator.mul());
//        System.out.println(calculator.dev());
//    }
//}
//