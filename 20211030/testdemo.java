import java.util.Arrays;
import java.util.PriorityQueue;

/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-10-25
 * Time: 19:05
 */
public class TestDemo {
    public static int[] topK(int array[], int temp) {//topk:一组数据当中，找到前K个最大的/最小的数据
        PriorityQueue<Integer> minHeap = new PriorityQueue<>();
        for(int i=0; i<array.length; i++) {//将最大的三个元素放入堆中（该堆为小堆）
            if(minHeap.size() < temp) {
                minHeap.offer(array[i]);
            } else {
                int t = minHeap.peek();
                if(t < array[i]) {
                    minHeap.poll();
                    minHeap.offer(array[i]);
                }
            }
        }
        int[] ret = new int[temp];
        for (int i = 0; i < temp; i++) {
            ret[i] = minHeap.poll();
        }
        return ret;
    }
    public static void main(String[] args) {
        int[] array = {10,6,8,48,72};
        int[] ret = topK(array,3);
        System.out.println(Arrays.toString(ret));
    }
    public static void main1(String[] args) {
        PriorityQueue<Integer> queue = new PriorityQueue<>();
        queue.offer(10);
        queue.offer(2);
        queue.offer(6);
        System.out.println(queue.peek());
        System.out.println(queue.poll());
        System.out.println(queue.peek());
        System.out.println(Arrays.toString(args));
    }

}
