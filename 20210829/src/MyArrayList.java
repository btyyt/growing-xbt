/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-08-29
 * Time: 15:24
 */
import java.util.Arrays;

public class MyArrayList {
    public int[] elem;//elem=null
    public int usedSize;
    public static  int capacity = 10;//数组大小
    public MyArrayList() {
        this.elem = new int[capacity];
        //this.usedSize = 0;
    }

    //判断1
    public boolean isFull() {
         /*if(this.usedSize == capacity) {
            return true;
        }
        return false;*/
        return this.usedSize==capacity;
    }

    //在 pos 位置新增元素
    public void add(int pos,int data) {
        if(pos<0||pos>this.usedSize) {
            System.out.println("pos位置不合法");
            return;
        }
        //1.满的情况
        if(isFull()) {
            this.elem=Arrays.copyOf(this.elem,2*capacity);
            capacity *= 2;
        }
        //2.pos的合法情况
        for(int i=this.usedSize-1; i >= pos; i--) {
            this.elem[i+1]=this.elem[i];
        }
        this.elem[pos]=data;
        this.usedSize++;
    }

    //打印顺序表
    public void display() {
        for(int i=0; i<this.usedSize; i++) {
            System.out.println(this.elem[i]+" ");
        }
        System.out.println();
    }

    //判断2(是否为空）
    public boolean isEmpty() {
        return this.usedSize == 0;
    }

    //判断是否包含某个元素
    public boolean contains(int toFind) {
        if(isFull()) return false;//判断是否满
        for(int i=0; i<this.usedSize; i++) {
            if(this.elem[i] == toFind) {
                return true;
            }
        }
        return false;
    }

    //查找某个元素对应的位置
    public int search(int toFind) {
        if(isEmpty()) return -1;
        for(int i=0; i<this.usedSize; i++) {
            if(this.elem[i]==toFind) {
                return i;
            }
        }
        return -1;
    }

    //获取 pos 位置的元素***
    public int getPos(int pos) {
        if (isEmpty()) {
            //return -1;
            throw new RuntimeException("顺序表是空的");//手动抛出错误（异常）
        }
        if(pos <0 ||pos >= this.usedSize) {
            throw new RuntimeException("pos不合法");//手动抛出错误（异常）
        }
        return this.elem[pos];
    }

    // 获取顺序表长度
    public int size() {
        return this.usedSize;
    }

    //给 pos 位置的元素设为value
    public void setPos(int pos, int value) {
        if(pos < 0 ||pos >=this.usedSize) {
            System.out.println("pos不合法！");
            return;
        }
        if(isEmpty()) {
            System.out.println("顺序表为空！");
            return;
        }
        this.elem[pos] = value;
    }

    //删除第一次出现的关键字key
    public void remove(int toRemove) {
        if(isEmpty()) return;
        int index = search(toRemove);
        if(index ==-1) {
            System.out.println("没有你要删除的数字");
            return ;
        }
        for (int i=index; i<this.usedSize-1; i++) {
            this.elem[i] =this.elem[i+1];
        }
        this.usedSize--;
    }

    //清空顺序表
    public void clear() {
        for(int i=0; i < this.usedSize; i++) {
            this.elem[i] = 0;
        }
        this.usedSize = 0;
    }

}
