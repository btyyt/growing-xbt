import java.util.Arrays;

/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-08-30
 * Time: 10:17
 */
public class TestDemo {
    public static void main(String[] args) {
        MyArrayList myArrayList = new MyArrayList();
        myArrayList.add(0,1);
        myArrayList.add(1,2);
        myArrayList.add(2,3);
        myArrayList.add(3,4);
        myArrayList.display();//打印顺序表
        System.out.println(myArrayList.contains(5));//判断一个元素是否存在

    }
    public static void swapArray(int[] array1,int[] array2) {
        int len = Math.min(array1.length,array2.length);

        for(int i=0; i<len; i++) {
            int tmp=array1[i];
            array1[i]=array2[i];
            array2[i]=tmp;
        }
    }
    public static void main1(String[] args) {
        int[] array1={1,3,5,7,9,11,13,15,17,19};
        int[] array2={2,4,6,8,10};
        swapArray(array1,array2);
        System.out.println(Arrays.toString(array1));
        System.out.println(Arrays.toString(array2));
    }
}
