/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-10-02
 * Time: 9:53
 */

import java.util.Arrays;



public class MyArrayList {
    public int[] elem;//顺序表
    public int unedSize;//顺序表大小
    public static int capacity = 10;//容量

    public MyArrayList (){
        this.elem =  new int[capacity];
    }

    //判断顺序表是否满了
    public boolean isFull() {
        return this.unedSize == capacity;
    }

    //在pos位置新增元素
    public void add(int pos, int data) {
        if(pos < 0 || pos > this.unedSize) {
            System.out.println("pos不合法");
        }
        //1.满的情况  2.pos合法问题
        if(isFull()) {
            this.elem = Arrays.copyOf(this.elem, 2*capacity);
            capacity *= 2;
        }
        for (int i=this.unedSize-1; i>=pos; i--) {
           this.elem[i+1] = this.elem[i];
        }
        this.elem[pos] = data;
        this.unedSize++;
    }

    //打印顺序表
    public void display() {
        for (int i=0; i<this.unedSize; i++) {
            System.out.println(this.elem[i]);
        }
    }

    //判定是否包含某个元素
    public boolean contains(int toFind) {
        for (int i=0; i<this.unedSize; i++) {
            if(this.elem[i] == toFind) {
                return true;
            }
        }
        return false;
    }

    //查找某个元素对应的位置
    public int search(int toFind) {
        if(isFull()) {
            return -1;
        }
        for (int i=0; i<this.unedSize; i++) {
            if(this.elem[i] == toFind) {
                return i;
            }
        }
        return -1;
    }

    //获取pos位置的元素
    public int getPos(int pos) {
        return this.elem[pos];
    }

    //将pos位置的元素设置为value
    public void setPos(int pos,int value) {
        this.elem[pos] = value;
    }

    //删除第一次出现的关键字key
    public void remove(int toRemove) {
        for (int i=0; i<this.unedSize; i++) {
            if(this.elem[i] == toRemove) {

            }
        }
    }

    //获取顺序表的长度
    public int size() {
        return this.unedSize;
    }

    //清空顺序表
    public void clear() {
        for (int i=0; i<this.unedSize; i++) {
            this.elem[i] = 0;
        }
        this.unedSize = 0;
    }
}