/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-11-09
 * Time: 17:08
 */
//1
//interface Graph{
//    void drawing();
//}
//class Rectangle implements Graph{
//    private double length;
//    private double weight;
//    public Rectangle(double length, double weight) {
//        this.length = length;
//        this.weight = weight;
//    }
//    public void drawing() {
//        System.out.println("画一个长方形，长："+this.length+"，宽："+this.weight);
//    }
//}
//class Circle implements Graph{
//    private double radius;
//    public Circle(double radius) {
//        this.radius = radius;
//    }
//    public void drawing() {
//        System.out.println("画一个圆，半径："+this.radius);
//    }
//}
//class Triangle implements Graph{
//    private double a;
//    private double b;
//    private double c;
//    public Triangle(double a, double b, double c) {
//        this.a = a;
//        this.b = b;
//        this.c = c;
//    }
//    public void drawing() {
//        System.out.println("画一个三角形，三边之长分别为："+this.a+' '+this.b+' '+this.c);
//    }
//}
//public class Test {
//    public static void myDrawing(Graph g) {
//        g.drawing();
//    }
//    public static void main(String []args) {
//        myDrawing(new Rectangle(10,5));
//        myDrawing(new Circle(8));
//        myDrawing(new Triangle(6,6,6));
//    }
//}
abstract class Equiment {
    private String name;
    public String getName() {
        return this.name;
    }
    public Equiment(String name) {
        this.name = name;
    }
}
class Fan extends Equiment {//电扇
    private double power;
    public Fan(String name, double power) {
        super(name);
        this.power = power;
    }
    public String toString() {
        return "电扇:"+this.getName()+"，power:"+this.power;
    }
}
class Refrigerator extends Equiment {//冰箱
    private double volume;
    public Refrigerator(String name, double volume) {
        super(name);
        this.volume = volume;
    }
    public String toString() {
        return "冰箱:"+this.getName()+"，容量:"+this.volume;
    }
}
class MyFactory {
    public static Equiment getInstance(String equimentName) {
        if(equimentName.equals("电扇"))
            return new Fan("小电扇",1000);
        else if(equimentName.equals("电冰箱"))
            return new Refrigerator("海尔冰箱",500);
        else
            return null;
    }
}
public class Test {
    public static void main(String [] args) {
        Equiment f=MyFactory.getInstance("电扇");
        System.out.println(f);
        Equiment e=MyFactory.getInstance("电冰箱");
        System.out.println(e);
    }
}