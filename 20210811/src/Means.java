import java.util.Scanner;

/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-08-11
 * Time: 11:00
 */

public class Means {
    public static void menu( ) {
        System.out.println("请根据选择使用你需要使用的功能：");
        System.out.println("1:求两个整数的最大值");
        System.out.println("2:求两个小数的最大值");
        System.out.println("3:求两个小数和一个整数的大");
    }
    public static int fun1(int a,int b) {
        if(a>b) {
            return a;
        } else {
            return b;
        }
    }
    public static double fun2(double n,double m) {
        if(n>m) {
            return n;
        } else {
            return m;
        }
    }
    public static double fun3(double c,double d) {
        double sum3=c+d;
        return sum3;
    }
    public static void fun4(double sum3,int p ) {
        double q=(double) p;
        if(sum3>q) {
            System.out.println(sum3+">"+p);
        } else {
            System.out.println(sum3+"<="+p);
        }
    }
    public static void main(String[] args) {
        menu();
        while (true) {
            Scanner sc=new Scanner(System.in);
            int s=sc.nextInt();
            switch(s) {
                case 1: {
                    int a=sc.nextInt();
                    int b=sc.nextInt();
                    int sum1=fun1(a,b);
                    System.out.println(sum1);
                    break;
                }
                case 2: {
                    double n=sc.nextDouble();
                    double m=sc.nextDouble();
                    double sum2=fun2(n,m);
                    System.out.println(sum2);
                    break;
                }
                case 3: {
                    double c=sc.nextDouble();
                    double d=sc.nextDouble();
                    double sum3=fun3(c,d);
                    System.out.println("两个小数之和为："+sum3);
                    int p=sc.nextInt();
                    fun4(sum3, p);
                    break;
                }
            }
        }
    }
}

//求和的重载
//public class Means {
//    public static int fun1(int a,int b) {
//        int sum=a+b;
//        return sum;
//    }
//    public static double fun2(double n,double m,double t) {
//        double s=n+m+t;
//        return s;
//    }
//    public static void main(String[] args) {
//        int a=1,b=2;
//        int sum=fun1(a,b);
//        System.out.println("a+b="+sum);
//        double n=1.0,m=2.0,t=3.0;
//        double s=fun2(n,m,t);
//        System.out.println("n+m+t="+s);
//    }
//}
