import java.util.Scanner;

/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-11-12
 * Time: 11:19
 */
//青蛙跳台阶问题 top1
public class Test {
    public static int jump(int n) {
        if (n==0) return 0;
        if (n==1) return 1;
        if (n==2) return 2;
        return jump(n-1)+jump(n-2);
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int sum = jump(n);
        System.out.println(sum);
    }
}
//递归求解汉诺塔问题 top2
    //c版本
    /*#include<stdio.h>
int hannuo(int num)
{
	if(1 == num)
		return 1;
	else
		return 2 * hannuo(num - 1) + 1;
}
int main()
{
	int num = 0;
	int n=0;
	scanf("%d\n",&n);
	while(n>0)
    {
        scanf("%d", &num);
	    int ret = hannuo(num);
	    printf("%d天\n",ret);
	    n--;
	}
	return 0;
}*/
//public class Test {
//    public static int TowersOfHanoi(int n) {
//        if(n == 1) {
//            return 1;
//        }
//        else {
//            return 2 * TowersOfHanoi(n- 1) + 1;
////            TowersOfHanoi(n-1,a,c,b);
////            n++;
////            TowersOfHanoi(n-1,b,a,c);
////            return n;
//        }
//    }
//    public static void main(String[] args) {
//        Scanner sc = new Scanner(System.in);
//        int n = sc.nextInt();
//        int sum = TowersOfHanoi(n);
//        System.out.println(sum);
//    }
//}
//递归求斐波那契数列的第 N 项 top3
//public class Test {
//    public static int Fib(int n) {
//        if(n == 0) {
//            return 0;
//        }
//        if(n == 1 || n == 2) {
//            return 1;
//        }
//        return Fib(n-2) + Fib(n-1);
//    }
//    public static void main(String[] args) {
//        Scanner sc = new Scanner(System.in);
//        int n = sc.nextInt();
//        int sum = Fib(n);
//        System.out.println(sum);
//    }
//}
