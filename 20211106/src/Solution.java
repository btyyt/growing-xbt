/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-11-06
 * Time: 19:27
 */
public class Solution {
    public boolean Find(int target, int [][] array) {
        if(array == null) {
            return false;
        }
        int i = 0;//行
        int j = array[0].length-1;//列
        while(i < array.length && j >= 0) {
            if(target < array[i][j]) {
                j--;
            } else if(target > array[i][j]) {
                i++;
            }
            else {
                return true;
            }
        }
        return false;
    }
}
