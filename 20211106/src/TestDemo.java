/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-11-06
 * Time: 10:50
 */
public class TestDemo {
    public static void main(String[] args) {
        //方式1
        String str = "Hello word";
        //方式2
        String str2 = new String("Hello word");
        //方式3
        char[] array = {'H','e','l','l','o','w','o','r','d'};
        String str3 = new String(array);
//        System.out.println(str == str2);//false
//        System.out.println(str == str3);//false
//        System.out.println(str2 == str3);//false
//        String str4 = "gaobo";
//        String str5 = "gao"+"bo";//字符串常量值，在堆内存
//        System.out.println(str4 == str5);
        String str6 = "gaobo";
        String str7 = "gao";
        String str8 =str7 + "bo";
//        System.out.println(str6 == str8);
        String str9 = "hello";
        String str10 = str9;
//        System.out.println(str10 == str9);
        str9 = "word";
//        System.out.println(str9);
//        System.out.println(str10);
        String str11 = new String("zjq");
        String str12 = new String("zjq");
        System.out.println(str11 == str12);
        System.out.println(str11.equals(str12));

    }
}
