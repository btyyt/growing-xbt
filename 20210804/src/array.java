import java.util.Arrays;

/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-08-04
 * Time: 10:57
 */
//java冒泡排序
//public class array {
//    public static void bubbleSort(int[] arr) {
//        int i=0,j=0;
//        int tmp;
//        for(i=0;i<arr.length-1;i++) {
//            for(j=0;j<arr.length-1-i;j++) {
//                if(arr[j]>arr[j+1]) {
//                    tmp=arr[j];
//                    arr[j]=arr[j+1];
//                    arr[j+1]=tmp;
//                }
//            }
//        }
//    }
//    public static void main(String[] args) {
//        int[] arr={1,2,4,6,7,3,5};
//        bubbleSort(arr);
//        System.out.println(Arrays.toString(arr));
//    }
//}
//public class array {
//    public static boolean isSorted(int[] arr) {
//        for(int i=0;i<arr.length-1;i++) {
//            if(arr[i]>arr[i+1]) {
//                return false;
//            }
//        }
//        return true;
//    }
//    public static void main(String[] args) {
//        int[] arr={1,2,3,7,5};
//        System.out.println(isSorted(arr));
//    }
//}
//二分查找
//public class array {
//    public static void main(String[] args) {
//        int[] arr={1,2,3,4,5,6,7};
//        int sum=binarySearch(arr,5);
//        System.out.println(sum);
//    }
//    public static int binarySearch(int[] arr,int s) {
//        int left=0;
//        int right=arr.length-1;
//        while(left<=right) {
//            int mid=(left+right)/2;
//            if(arr[mid]>s) {
//                right=mid-1;
//            } else if(arr[mid]<s) {
//                left=mid+1;
//            }
//            if(arr[mid]==s){
//                return mid;
//            }
//        }
//        return -1;
//    }
//}

//public class array {
//    public static String myToString(int[] array) {
//        String ret="[";
//        for(int i=0;i<array.length;i++) {
//            ret+=array[i];
//            if(i!=array.length-1) {
//                ret+=",";
//            }
//        }
//        ret+="]";
//        return ret;
//    }
//    public static void main(String[] args) {
//        int[] array={1,2,3};
//        System.out.println(myToString(array));
//    }
//}

//public class array {
//    public static void main(String[] args) {
//        int[] arr={1,2,3};
//        System.out.println(Arrays.toString(copyOf(arr)));
//    }
//    public static int[] copyOf(int[] arr) {
//        int[] ret=new int[arr.length];
//        for(int i=0;i<arr.length;i++) {
//            ret[i]=arr[i];
//        }
//        return ret;
//    }
//}