 #include<stdio.h>
void mppx(int a[],int s)
{
    int i,j;
    int t;
    for(i=0;i<s-1;i++)
    {
        int f=1;//假设在一趟排序后数据已经有序
        for(j=0;j<s-1-i;j++)
        {
            if(a[j]>a[j+1])
            {
                t=a[j];a[j]=a[j+1];a[j+1]=t;
                f=0;//当f=0时，表示数组还没有有序
            }
        }
        if(f==1)//当有序的时候就直接跳出来，提高效率
        {
            break;
        }
    }
}
int main()
{
    int a[]={10,9,8,7,6,5,4,3,2,1};
    int i;
    int s=sizeof(a)/sizeof(a[0]);
    mppx(a,s);
    for(i=0;i<s;i++)
    {
        printf("%d ",a[i]);
    }
    return 0;
}


