#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int main()
{
	int a = 3;        //00000000000000000000000000000011
	int b = 5;        //00000000000000000000000000000101
	int c1 = a & b;   //00000000000000000000000000000001  ��1
	int c2 = a | b;   //00000000000000000000000000000111  ��7
	int c3 = a ^ b;   //00000000000000000000000000000110  ��6
	printf("c1 = %d\n", c1);
	printf("c2 = %d\n", c2);
	printf("c3 = %d\n", c3);
	return 0;
}
