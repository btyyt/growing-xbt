#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<assert.h>
char * my_strcpy(char *dst, const char *src)
{
	char *cp = dst;
	assert(dst&&src);
	while (*cp++ = *src++)
	{
		;
	}
	return(dst);
}

int main()
{
	char arr[10] = { 'a', 'b', 'c', 'd', 'e', '\0' };
	my_strcpy(arr, "hello");
	printf("%s\n", arr);
	return 0;
}
