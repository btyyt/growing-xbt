#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
int my_strlen(const char *a)
{
	int count = 0;
	while (*a)
	{
		count++;
		a++;
	}
	return count;
}
int main()
{
	char a[10] = "abcdefg";
	int s = my_strlen(a);
	printf("%d\n", s);
	return 0;
}
