/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-08-03
 * Time: 14:37
 */
public class array {
    public static String myToString(int[] array) {
        String ret="[";
        for(int i=0;i<array.length;i++) {
            if(i!=array.length) {
                ret+=array[i];
            }
        }
        ret+="]";
        return ret;
    }
    public static void main(String[] args) {
        int[] array={1,2,3,4,5};
        System.out.println(myToString(array));
    }
}
