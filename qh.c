#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int Sn(int m)
{
	int i;
	int sum=0;
	int s= m;
	if (m <= 0 || m > 10)
	{
		return 0;
	}
	else
	{
		for (i = 0; i < 5; i++)
		{
			sum = sum + s;
			s = s * 10 + m;
		}
	}
	return sum;
}
int main()
{
	int n;
	scanf("%d", &n);
	int m = Sn(n);
	printf("%d\n", m);
	return 0;
}