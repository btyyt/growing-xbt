/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-11-10
 * Time: 15:33
 */
abstract class Person {
    protected int id;
    protected String name;
    protected String sex;
    protected String birthday;
    public static String nationality = "中国";
    Person() {

    }

    public Person(int id, String name, String sex, String birthday) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", birthday='" + birthday + '\'' +
                '}';
    }

    public void saying(Person  per, String msg) {
        System.out.println("向"+per.name+"说了:"+msg);
    }
    public abstract void working(String information) ;
}
class Student extends Person {
    public int studentID;
    public String className;
    public static String schoolName = "吉首大学";
    Student() {

    }
    Student(int id, String name, String sex, String birthday) {
        super(id,name,sex,birthday);
    }

    public Student(int studentID, String className) {
        this.studentID = studentID;
        this.className = className;
    }

    public Student(int id, String name, String sex, String birthday, int studentID, String className) {
        super(id, name, sex, birthday);
        this.studentID = studentID;
        this.className = className;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", birthday='" + birthday + '\'' +
                ", studentID=" + studentID +
                ", className='" + className + '\'' +
                '}';
    }
    public void takeLesson(String course) {
        System.out.println(this.name+"选修了"+course);
    }
    public void working(String information) {
        System.out.println(this.name+"正在学习："+information);
    }

}
class Teacher extends Person {
    public int teacherID;
    public String className;
    public int salary;
    public static String schoolName;
    Teacher() {

    }
    Teacher(int id, String name, String sex, String birthday) {
        super(id,name,sex,birthday);
    }

    public Teacher(int teacherID, String className, int salary) {
        this.teacherID = teacherID;
        this.className = className;
        this.salary = salary;
    }

    public Teacher(int id, String name, String sex, String birthday, int teacherID, String className, int salary) {
        super(id, name, sex, birthday);
        this.teacherID = teacherID;
        this.className = className;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", birthday='" + birthday + '\'' +
                ", teacherID=" + teacherID +
                ", className='" + className + '\'' +
                ", salary=" + salary +
                '}';
    }

    public void working(String information) {
        System.out.println(this.name+"正在准备："+information);
    }
    public void teaching(Student student,String course) {
        System.out.println("正在为"+student.name+"教授"+course);
    }
}
public class Test1 {
    public static void main(String[] args) {
        //public Student(int id, String name, String sex, String birthday, int studentID, String className) {
        Student person = new Student(010,"孙悟空","男","11.11",001,"5ban");
        person.saying(person,"傻瓜");
        System.out.println(person.toString());
        person.working("体育课");
        person.takeLesson("体育课程");
        //public Teacher(int id, String name, String sex, String birthday, int teacherID, String className, int salary)
        Teacher teacher = new Teacher(020,"菩提祖师","男","10.10",002,"6ban",6000);
        System.out.println(teacher.toString());
        teacher.teaching(person,"体育课");
        teacher.working("体育课内容");
        System.out.println(person.nationality);
        System.out.println(person.schoolName);
    }
}
