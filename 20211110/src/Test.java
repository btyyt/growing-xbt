/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-11-10
 * Time: 14:29
 */
class Animal{
    protected String name;
    protected int age;
    protected String sex;
    protected int legNum;
    protected int weight;
    Animal()  {

    }

    public Animal(String name, int age, String sex, int legNum, int weight) {
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.legNum = legNum;
        this.weight = weight;
    }
    protected void eating(String food) {
        System.out.println("正在吃东西"+food);
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                ", legNum=" + legNum +
                ", weight=" + weight +
                '}';
    }
}
class Pig extends Animal {
    protected int length;
    protected int height;
    protected String color;
    public Pig(String name, int age, String sex, int legNum, int weight) {
        super(name, age, sex, legNum, weight);
    }

    public Pig(int length, int height, String color) {
        this.length = length;
        this.height = height;
        this.color = color;
    }

    public Pig(String name, int age, String sex, int legNum, int weight, int length, int height, String color) {
        super(name, age, sex, legNum, weight);
        this.length = length;
        this.height = height;
        this.color = color;
    }

    @Override
    public String toString() {
        return "Pig{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                ", legNum=" + legNum +
                ", weight=" + weight +
                ", length=" + length +
                ", height=" + height +
                ", color='" + color + '\'' +
                '}';
    }
    public void eating(String food) {
        System.out.println("Pig正在吃："+food);
    }
    public void walking() {
        System.out.println("Pig可以行走");
    }
}
class Chicken extends Animal {
    public String combColor;
    public Chicken(String name, int age, String sex, int legNum, int weight) {
        super(name, age, sex, legNum, weight);
    }

    public Chicken(String combColor) {
        this.combColor = combColor;
    }

    public Chicken(String name, int age, String sex, int legNum, int weight, String combColor) {
        super(name, age, sex, legNum, weight);
        this.combColor = combColor;
    }

    @Override
    public String toString() {
        return "Chicken{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                ", legNum=" + legNum +
                ", weight=" + weight +
                ", combColor='" + combColor + '\'' +
                '}';
    }
    public void eating(String food) {
        System.out.println("Chicken正在吃："+food);
    }
    public void flying() {
        System.out.println("鸡可以飞");
    }
}
public class Test {
    public static void main(String[] args) {
        Pig pig = new Pig("peiqi",6,"雄",4,100,100,50,"粉色");
        System.out.println(pig.toString());
        pig.eating("白菜");
        pig.walking();
        Chicken chicken = new Chicken("xhj",1,"雌",2,10,"红色");
        System.out.println(chicken.toString());
        chicken.eating("虫子");
        chicken.flying();
    }
}
