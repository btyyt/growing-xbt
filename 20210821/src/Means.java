import java.util.Random;
import java.util.Scanner;
/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-08-21
 * Time: 9:49
 */

//for-each(类型：数组名） {

//}
public class Means {
    public static void main(String[] args) {
        int[] arr={1,2,3};
        for (int x:arr) {
            System.out.println(x);
        }
    }
}

//public class Means {
//    public static void main(String[] args) {
//        int[] arr={1,2,3};
//        System.out.println("数组的长度="+arr.length);//arr.length是指数组长度，可以直接使用
//        System.out.println("arr[1]="+arr[1]);
//    }
//}

//int arr =new int[]{1,2,3};
//
//int arr={1,2,3};
//
//int arr[]={1,2,3};//这个定义与C语言一致，不建议


//public class Means {
//    public static void main(String[] args) {
//        Scanner sc=new Scanner(System.in);
//
//        Random random =new Random();
//        int rand =(random.nextInt(100)+1)/10;
//
//        char[] arr={'我','是','地','球','的','超','级','英','雄','呀'};
//        int s=0;
//        for (int n=0;n<arr.length;n++) {
//            System.out.print(arr[n]);
//            s++;
//        }
//        System.out.println(rand);
//        for(int j=0;j<10;j++) {
//            int a = rand%s;
//            int b = 5;
//            char c=arr[b];
//            arr[b]=arr[a];
//            arr[a]=c;
//        }
//        System.out.println(arr);
//    }
//}
