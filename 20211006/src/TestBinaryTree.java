import com.sun.org.apache.regexp.internal.RE;
/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-10-06
 * Time: 23:58
 */
class Node {
    public char val;
    public Node left;
    public Node right;

    public Node(char val) {//构造方法
        this.val = val;
    }
}
public class TestBinaryTree {
    //穷举的方式创建二叉树
    public Node createTree() {
        Node A = new Node('A');
        Node B = new Node('B');
        Node C = new Node('C');
        Node D = new Node('D');
        Node E = new Node('E');
        Node F = new Node('F');
        Node G = new Node('G');
        Node H = new Node('H');
        A.left = B;
        A.right = C;
        B.left = D;
        B.right = E;
        C.left = F;
        C.right = G;
        E.right = H;
        return A;
    }
    //二叉树的前序遍历
    void preOrderTraversal(Node root) {
        if(root == null) {
            return;
        }
        System.out.print(root.val);
        preOrderTraversal(root.left);
        preOrderTraversal(root.right);
    }
    //二叉树的中序遍历
    void inOrderTraversal(Node root) {
        if(root == null) {
            return;
        }
        inOrderTraversal(root.left);
        System.out.print(root.val);
        inOrderTraversal(root.right);
    }
    //二叉树的后序遍历
    void postOrderTraversal(Node root) {
        if(root == null) {
            return;
        }
        postOrderTraversal(root.left);
        postOrderTraversal(root.right);
        System.out.print(root.val);
    }
    //遍历思路-求结点个数
    static int size = 0;
    void getSizel(Node root) {
        if(root == null) {
            return;
        }
        size++;
        getSizel(root.left);
        getSizel(root.right);
    }
    //子问题思路-求结点个数
    int getSize2(Node root) {
        if(root == null) {
            return 0;
        }
        int val = getSize2(root.left)+getSize2(root.right)+1;
        return val;
    }
    //遍历思路-求叶子节点个数
    static int leafSize = 0;
    void getLeafSize1(Node root) {

    }
    //子问题思路-求叶子节点个数
    int getLeafSize2(Node root) {

    }
    //子问题思路-求第K层结点个数
    int getKLevwSize(Node root) {

    }
    //获取二叉树的高度
    int getHeight(Node root) {

    }
}
