/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-10-06
 * Time: 23:58
 */
public class TestDemo {
    public static void main(String[] args) {
        TestBinaryTree testBinaryTree = new TestBinaryTree();
        Node root = testBinaryTree.createTree();
        testBinaryTree.preOrderTraversal(root);//前序遍历
        System.out.println();
        testBinaryTree.inOrderTraversal(root);//中序遍历
        System.out.println();
        testBinaryTree.postOrderTraversal(root);//后序遍历
        System.out.println();
        testBinaryTree.getSizel(root);
        System.out.println(TestBinaryTree.size);//遍历思路-求结点个数
        System.out.println(testBinaryTree.getSize2(root)); //子问题思路-求结点个数
    }
}
