//test.c
#define _CRT_SECURE_NO_WARNINGS 1

#include"game.h"
void menu()
{
	printf("******欢迎使用三子棋**********\n");
	printf("******************************\n");
	printf("*****     1.人vs电脑     *****\n");
	printf("*****     2.人vs人       *****\n");
	printf("*****     0.结束游戏     *****\n");
	printf("******************************\n");
}
void game()
{
	char board[ROW][COL] = { 0 };//存储数据——>二维数组

	InitBoard(board, ROW, COL);//初始化棋盘-->初始化空格

	DisplayBoard(board, ROW, COL);//打印棋盘
	char ret = 0;
	while (1)
	{    
		//玩家下棋
		PlayerMove(board, ROW, COL);
		DisplayBoard(board, ROW, COL);
		//判断玩家是否赢得游戏
		ret = IsWin(board, ROW, COL);
		if (ret != 'C')
			break;
		//电脑下棋
		ComputerMove(board, ROW, COL);
		DisplayBoard(board, ROW, COL);
		//判断电脑是否赢得比赛
		ret = IsWin(board, ROW, COL);
		if (ret != 'C')
			break;
	}
	if (ret == '*')
	{
		printf("玩家1赢啦\n");
	}
	else if (ret == '#')
	{
		printf("电脑赢啦\n");
	}
	else
	{
		printf("平局\n");
	}
	DisplayBoard(board, ROW, COL);


}
void game_2()
{
	char board[ROW][COL] = { 0 };//存储数据——>二维数组

	InitBoard(board, ROW, COL);//初始化棋盘-->初始化空格

	DisplayBoard(board, ROW, COL);//打印棋盘
	char ret = 0;
	while (1)
	{
		//玩家1下棋
		PlayerMove(board, ROW, COL);
		DisplayBoard(board, ROW, COL);
		//判断玩家1是否赢得游戏
		ret = IsWin(board, ROW, COL);
		if (ret != 'C')
			break;
		//玩家2下棋
		PlayerMove_2(board, ROW, COL);
		DisplayBoard(board, ROW, COL);
		//判断玩家2是否赢得比赛
		ret = IsWin(board, ROW, COL);
		if (ret != 'C')
			break;
	}
	if (ret == '*')
	{
		printf("玩家1赢啦\n");
	}
	else if (ret == '#')
	{
		printf("玩家2赢啦\n");
	}
	else
	{
		printf("平局\n");
	}
	DisplayBoard(board, ROW, COL);


}
int main()
{
	int input = 0;
	srand((unsigned int)time(NULL));
	do
	{
		menu();
		printf("请选择：>");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			game();
			break;
		case 2:
			game_2();
			break;
		case 0:
			printf("退出游戏\n");
			break;
		default:
			printf("选择错误，请重新选择\n");
			break;
		}
	} while (input);
	return 0;
}
//game.c
#define _CRT_SECURE_NO_WARNINGS 1
#include"game.h"
void InitBoard(char board[ROW][COL], int row, int col)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < row; i++)
	{
		for (j = 0; j < col; j++)
		{
			board[i][j] = ' ';
		}
	}
}
void DisplayBoard(char board[ROW][COL], int row, int col)
{
	int i = 0;
	for (i = 0; i < row; i++)
	{
		int j = 0;
		for (j = 0; j < col; j++)
		{
			printf(" %c ", board[i][j]);
			if (j < col - 1)
				printf("|");
		}
		printf("\n");
		if (i < row - 1)
		{
			int j = 0;
			for (j = 0; j < col; j++)
			{
				printf("---");
				if (j < col - 1)
					printf("|");
			}
			printf("\n");
		}
	}
}
void PlayerMove(char board[][COL], int row, int col)//玩家1下棋
{
	int x = 0;
	int y = 0;
	printf("玩家1走：>\n");
	while (1)
	{
		printf("请输入下棋的坐标：>");
		scanf("%d %d", &x, &y);
		if (x >= 1 && x <= row&&y >= 1 && y <= col)
		{
			if (board[x - 1][y - 1] == ' ')
			{
				board[x - 1][y - 1] = '*';
				break;
			}
			else
			{
				printf("坐标被占用，请重新输入\n");
			}
		}
		else
		{
			printf("坐标非法，请重新输入\n");
		}
	}
}
void PlayerMove_2(char board[][COL], int row, int col)//玩家2下棋
{
	int x = 0;
	int y = 0;
	printf("玩家2走：>\n");
	while (1)
	{
		printf("请输入下棋的坐标：>");
		scanf("%d %d", &x, &y);
		if (x >= 1 && x <= row&&y >= 1 && y <= col)
		{
			if (board[x - 1][y - 1] == ' ')
			{
				board[x - 1][y - 1] = '#';
				break;
			}
			else
			{
				printf("坐标被占用，请重新输入\n");
			}
		}
		else
		{
			printf("坐标非法，请重新输入\n");
		}
	}
}
void ComputerMove(char board[ROW][COL], int row, int col)//电脑下棋
{
	int x = 0;
	int y = 0;
	printf("电脑走：>\n");
	while (1)
	{
		int x = rand() % row;
		int y = rand() % col;
		if (board[x][y] == ' ')
		{
			board[x][y] = '#';
			break;
		}
	}
}
int IsFull(char board[ROW][COL], int row, int col)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < row; i++)
	{
		for (j = 0; j < col; j++)
		{
			
			
				return 0;
			
		}
	}
	return 1;
}
char IsWin(char board[ROW][COL], int row, int col)
{
	int i = 0;
	for (i = 0; i < row; i++)//判断行
	{
		if (board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0] != ' ')
		{
			return board[i][1];
		}
	}
	for (i = 0; i < col; i++)//判断列
	{
		if (board[0][i] == board[1][i] && board[1][i] == board[2][i] && board[1][i] != ' ')
		{
			return board[1][i];
		}
	}

	//判断对角线
	if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[1][1] != ' ')
	{
		return board[1][1];
	}
	if (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[1][1] != ' ')
	{
		return board[1][1];
	}

	//判断平局
	//如果棋盘满了返回1， 不满返回0
	int ret = IsFull(board, row, col);
	if (ret == 1)
	{
		return 'Q';
	}

	//继续
	return 'C';
}


//game.h
#pragma once

#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#define ROW 3
#define COL 3

void InitBoard(char board[ROW][COL], int row, int col);//制空数组
void DisplayBoard(char board[ROW][COL], int row, int col);//制定空表
void PlayMove(char board[ROW][COL],int row, int col);//玩家1
void PlayMove_2(char board[ROW][COL], int row, int col);//玩家2
void ComputerMove(char board[ROW][COL], int row, int col);//电脑
char IsWin(char board[ROW][COL], int row, int col);//判断
int IsFull(char board[ROW][COL], int row, int col);//查看表格是否为满
