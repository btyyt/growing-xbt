/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-10-03
 * Time: 21:54
 */
public class Test {
    public static void main(String[] args) {
        MyLinkedList myLinkedList = new MyLinkedList();
        //myLinkedList.createList();
        myLinkedList.addFirst(1);//头插法进入数据
        myLinkedList.addFirst(2);
        myLinkedList.addFirst(3);
        myLinkedList.addFirst(4);
        myLinkedList.addFirst(4);
        myLinkedList.addLast(5);
        myLinkedList.show();
        System.out.println("单链表的长度为："+myLinkedList.size());
        System.out.println(myLinkedList.contains(5));
        myLinkedList.addIndex(3,10);//index是数组的下标，数组下标从0开始
        myLinkedList.show();
        System.out.println("=======");
//        myLinkedList.remove(4);
//        myLinkedList.remove(2);
//        myLinkedList.show();
        myLinkedList.removeAllKey(4);
        myLinkedList.show();
        myLinkedList.clear();
    }
}
