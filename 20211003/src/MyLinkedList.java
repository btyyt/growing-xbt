/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-10-03
 * Time: 17:13
 */
class Node {
    public int val;//节点
    public Node next;//Null

    public Node(int val) { //构造方法
        this.val = val;
    }
}
//链表
public class MyLinkedList {
    public Node head;//标识单链表的头节点

    public void createList() {
        Node node1 = new Node(12);
        Node node2 = new Node(3);
        Node node3 = new Node(5);
        Node node4 = new Node(2);

        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        this.head = node1;
    }
    //打印链表
    public void show() {
        Node cur = this.head;
        while (cur != null) {
            System.out.println(cur.val+" ");
            cur = cur.next;
        }
    }

    //得到单链表的长度
    public int size() {
        Node cur = this.head;
        int count = 0;//计算单链表的长度
        while (cur != null) {
            count++;
            cur = cur.next;
        }
        return count;
    }

    //查找是否包含关键字key是否在单链表当中
    public boolean contains(int key) {
        Node cur = this.head;
        while (cur != null) {
            if(cur.val == key) {
                return true;
            }
            cur = cur.next;
        }
        return false;
    }

    //头插法（*****）
    public void addFirst(int data) {
        Node node = new Node(data);
        if (this.head == null) {//第一次插入
            this.head = node;
        }else {
            node.next = this.head;//不是第一次
            this.head = node;
        }
    }

    //尾插法
    public void addLast(int data) {
        Node node = new Node(data);
        if (this.head == null) {
            this.head = node;
        }else {
            Node cur = this.head;
            while (cur.next != null) {
                cur = cur.next;
            }
            cur.next = node;
//            while (cur != null) {
//                if(cur.next == null) {
//                    cur.next = node;
//                    return ;
//                }
//                cur = cur.next;
//            }
        }
    }

    //找到index的前一个节点
    public Node searchPrev(int index) {
        Node cur = this.head;
        int count=0;
        while (count != index-1) {
            cur = cur.next;
            count++;
        }
        return cur;
    }
    //任意位置插入,第一个数据节点为0号下标.从中间插入一个数据
    public void addIndex(int index,int data) {
        if(index < 0 || index > size() ) {
            System.out.println("index不合法");
            return;
        }
        //头插法
        if(index == 0) {
            addFirst(data);
            return;
        }
        //尾插法
        if (index == size()) {
            addLast(data);
        }
        //
        Node cur = searchPrev(index);
        Node node = new Node(data);
        node.next = cur.next;
        cur.next = node;
    }

    //查找关键字key的前一个节点
    public Node searchPrevNode(int val) {
        Node cur = this.head;
        while (cur.next != null) {
            if(cur.next.val == val) {
                return cur;
            }
            cur = cur.next;
        }
        return null;
    }
    //删除第一次出现关键字为key的节点
    public void remove(int val) {
        if(this.head == null) {
            return;
        }
        //单独判断头节点
        if (this.head.val == val) {
            this.head = this.head.next;
            return;
        }
        Node cur = searchPrevNode(val);
        if (cur == null) {
            System.out.println("链表里面没有关键字key");
            return;
        }
        Node del = cur.next;
        cur.next = del.next;
    }

    //删除所有值为key的节点
    public void removeAllKey(int val) {
        if (this.head == null) {
            return;
        }
        Node prev = this.head;
        Node cur = this.head.next;

        while (cur != null) {
            if (cur.val == val) {
                prev.next = cur.next;
                cur = cur.next;
            } else {
                prev = cur;
                cur = cur.next;
            }
        }
        //最后判断头节点
        if(this.head.val == val) {
            this.head = this.head.next;
        }
    }

    //清空链表
    public void clear() {
        while (this.head != null) {
            Node curNext = this.head.next;
            this.head.next = null;
            this.head = curNext;
        }
        System.out.println("清空单链表了");
    }
}
