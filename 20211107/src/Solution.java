/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-11-07
 * Time: 20:19
 */
/*
有一个长度为 n 的非降序数组，比如[1,2,3,4,5]，将它进行旋转，即把一个数组最开始的若干个元素搬到数组的末尾，变成一个旋转数组，比如变成了[3,4,5,1,2]，或者[4,5,1,2,3]这样的。请问，给定这样一个旋转数组，求数组中的最小值。

数据范围：1 \le n \le 100001≤n≤10000，数组中任意元素的值: 0 \le val \le 100000≤val≤10000
要求：空间复杂度：O(1)O(1) ，时间复杂度：O(logn)O(logn)
示例1
输入：
[3,4,5,1,2]
复制
返回值：
1
复制
示例2
输入：
[3,100,200,3]
复制
返回值：
3
 */
import java.util.ArrayList;
public class Solution {
    public static int minNumberInRotateArray(int [] array) {
        if(array == null || array.length == 0) {
            return 0;
        }
        int left = 0;
        int right = array.length-1;
        int mid = 0 ;
        while(array[left] >= array[right]) {
            if(right - left == 1) {
                mid = right;
                break;
            }
            if(array[right] == array[left] && array[left] == array[mid]) {
                int result = array[left];
                for(int i=left+1; i<right ; i++) {
                    if(array[i] < result) {
                        result = array[i];
                    }
                }
                return result;
            }
            mid = (left + right)/2;
            if(array[mid] >= array[left]) {
                left = mid;
            } else {
                right = mid;
            }
        }
        return array[mid];
    }
    public static void main(String[] args) {
        int[] array = {3,4,5,1,2};
        int sum = minNumberInRotateArray(array);
        System.out.println(sum);
    }
}
//public class Solution {
//    public static int minNumberInRotateArray(int [] array) {
//        if(array == null || array.length == 0) {
//            return 0;
//        }
//        for(int i=0; i<array.length-1; i++) {
//            if(array[i] > array[i+1]) {
//                return array[i+1];
//            }
//        }
//        return array[0];
//    }
//
//    public static void main(String[] args) {
//        int[] array = {3,4,5,1,2};
//        int sum = minNumberInRotateArray(array);
//        System.out.println(sum);
//    }
//}