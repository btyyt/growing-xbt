#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<malloc.h>
#include<time.h>

typedef struct guest
{
	char name[20];//姓名
	char  id[20];//身份证号
	char  tel[25];//电话
	char adr[20];//地址
	int year;
	int mou;
	int day;
	int sw;//编号
	struct guest *next;
}g,*pointer;

g guest[200];//结构体数组

int count = 0, dealine = 7;//全局变量，预定量

//void menu()初始菜单
//void menu_1()用户菜单
//void menu_2()管理员菜单

//void filesave()保存数据到文件
//void fileread()读取文件

//void add(point *head)//添加用户信息

//void revise()修改客户信息
//void del()删除客户信息
//void search()查找客户信息
//void show(point head)//浏览客户信息

void menu()//初始菜单
{
	printf("\n \n                   ☆—————☆————————☆————————☆—————☆\n");
	printf("                   │ ☆     ☆           欢迎来到口罩预约系统！      ☆      ☆│-☆\n");
	printf("                   │  ———————————————————————————— │ │\n");
	printf("                   │                      1.用户                               │ │\n");
	printf("                   │                      2.管理员                             │ │\n");
	printf("                   │                      3.退出口罩预约系统                   │ │\n");
	printf("                   ☆————☆—————————☆—————————☆————☆│\n");
	printf("                    请选择:");
}

void menu_1()//用户菜单
{
	printf("\n\n                    ============================================================\n");
	printf("                   │ ☆     ☆               用户菜单               ☆      ☆ │-☆\n");
	printf("                   │                                                           │ │\n");
	printf("                   │                    1.预约口罩(填写个人信息)               │ │\n");
	printf("                   │                    2.返回上一级                           │ │\n");
	printf("                   │                    3.退出口罩预约系统                     │ │\n");
	printf("                    ============================================================ │\n");
	printf("                   请选择:");
}

void menu_2()//管理员菜单
{
	printf("\n\n                    ============================================================\n");
	printf("                   │ ☆     ☆               管理员菜单              ☆      ☆│-☆\n");
	printf("                   │                                                           │ │\n");
	printf("                   │       1.修改客户信息               2.删除客户信息         │ │\n");
	printf("                   │                                                           │ │\n");
	printf("                   │       3.查询客户信息               4.浏览客户信息         │ │\n");
	printf("                   │                                                           │ │\n");
	printf("                   │                      5.返回上一级                         │ │\n");
	printf("                   │                                                           │ │\n");
	printf("                   │                    0.退出口罩预约系统                     │ │\n");
	printf("                    ============================================================ │\n");
	printf("                   请选择:");
}
pointer fileread()//读取文件
{
	pointer head = (pointer)malloc(sizeof(g));
	pointer p, q;
	p = q = head;
	FILE *fp;
	if ((fp = fopen("客户信息.txt", "r+")) == NULL)
	{
		return;
	}
	char tel[20], name[20], id[20], adr[20];
	int y, m, d, b;
	if (fscanf(fp, "%s %s %s %s %d %d %d %d", id, name, tel, adr, &y, &m, &d, &b)!=EOF)
	{
		strcpy(head->tel, tel);
		strcpy(head->name, name);
		strcpy(head->adr, adr);
		strcpy(head->id, id);;
		head->year = y; head->mou = m; head->day = d, head->sw = b;
		count++;
	}
	while (fscanf(fp, "%s %s %s %s %d %d %d %d", id, name, tel, adr, &y, &m, &d, &b) != EOF)
	{
		q = (pointer)malloc(sizeof(g));
		strcpy(q->tel, tel);
		strcpy(q->name, name);
		strcpy(q->adr, adr);
		strcpy(q->id, id);
		q->year = y; q->mou = m; q->day = d; q->sw = b; count++;
		p->next = q;
		p = q;
	}
	p->next = NULL;
	fclose(fp);
	return head;
}
void filesave(pointer head)//保存数据到文件
{
	pointer p;
	p = head;
	FILE *fp = fopen("客户信息.txt", "w");
	if (fp == NULL)
	{
		printf("\n\n                   ☆                   文件不能打开！                  ☆ \n");
		exit(0);
	}
	while (p != NULL)
	{
		fprintf(fp, "%-22s%-12s%-15s%-28s %d %d %d %d\n", p->id, p->name, p->tel, p->adr, p->year, p->mou, p->day, p->sw);
		p = p->next;
	}
	fclose(fp);
}
void add(pointer *head)//添加用户信息
{
	time_t timep;
	struct tm *t;
	time(&timep);
	t = gmtime(&timep);

	char a[20];//身份证数组
	int b, c, d, B, C, D,h;
	int i = 0, s = 0, flag = 1;
	pointer p, q, r;
	p = q = *head;

	FILE *fp;
	if ((fp = fopen("history.txt", "r")) == NULL)
	{
		fp = fopen("history.txt", "w");
		fclose(fp);
	}
	fp = fopen("history.txt", "r");
	system("cls");

	printf("                     ☆—————————————————————————————☆\n");
	printf("                     请输入个人的信息:\n");
	printf("                     输入身份证号码：");
	scanf("%s", a);

	if (strlen(a) != 18)
	{
		printf("                     请输入正确的身份证号码：");
		scanf("%s", a);
	}

	b = 1900 + t->tm_year; c = 1 + t->tm_mon; d = t->tm_mday;

	while (p != NULL)
	{
		if (strcmp(p->id, a) == 0)
		{
			printf("\n                     已有相同身份证号码，不可重复预定！");
			system("pause");
			return;
		}
		else
		{
			q = p; p = p->next;
		}
	}

	while (!feof(fp) && flag)
	{
		fscanf(fp, "%s %d %d %d %d", &guest[i].id, &guest[i].sw, &guest[i].year, &guest[i].mou, &guest[i].day);
		i++;
		s++;
	}
	for (i = 0; i < s; i++)
	{
		if (strcmp(guest[i].id, a) == 0)
		{
			B = guest[i].year; C = guest[i].mou; D = guest[i].day;
			flag = 0;
			break;
		}
	}

	r = (pointer)malloc(sizeof(g));
	r->next = NULL;
	if (r == NULL)
	{
		printf("\n                     空间分配失败！\n请稍后再试!\n");
		return;
	}
	if (q == NULL)
		*head = r;
	else
	{
		q->next = r;
	}
	strcpy(r->id, a);
	printf("                     请输入您的姓名：");
	scanf("%s", r->name);
	printf("                     请输入您的手机号码：");
	scanf("%s", &r->tel);
	printf("                     请输入您当前居住地址：");
	scanf("%s", r->adr);
	r->year = b; r->mou = c; r->day = d;
	count++;
	h = count;
	r->sw = h;
	system("cls");
	printf("           ★☆★☆★☆★☆★☆★☆★☆★☆★☆★☆☆★☆★☆★☆★☆★☆★☆★☆★☆☆★☆★☆★☆★☆★☆★☆\n");
	printf("                                           预约成功！您的编号为 %2d。\n", h);
	printf("                                      以下为您的个人信息，请确认是否正确，\n");
	printf("                                      若出现错误，请在12小时内联系管理员。\n");
	printf("           ☆————————————————————————————————————————————————★\n");
	printf("            编号    身份证号码            姓名        手机号码       地址                        预约时间\n");
	printf("            %-8d%-22s%-12s%-15s%-28s%4d年%2d月%2d日\n", r->sw, r->id, r->name, r->tel, r->adr, r->year, r->mou, r->day);
	printf("           ☆————————————————————————————————————————————————★\n");
	system("pause");
	system("cls");
}
void revise(pointer head)//修改客户信息
{
	int flag = 1,c=0;
	char id[20];
	pointer p;
	p = head;
	printf("\n                     ☆—————————————————————————————☆\n");
	printf("                      请输入准备修改的信息的身份证号码：");
	scanf("%s", &id);
	while (p!=NULL)
	{
		if (strcmp(p->id, id) == 0)
		{
			flag = 0;
			printf("\n                     ☆———                                              ———☆\n");
			printf("                                       1.身份证号码    2.姓名    \n\n");
			printf("                                       3.电话          4.地址\n\n");
			printf("                      请选择要修改的内容：");
			scanf("%d", &c);
			switch (c)
			{
			case 1:
				printf("                     ☆—————————————————————————————☆\n");
				printf("                      请输入要修改的身份证：");
				scanf("%s", p->id);
				printf("\n                      修改成功！\n"); break;
			case 2:
				printf("                     ☆—————————————————————————————☆\n");
				printf("                      请输入要修改姓名：");
				scanf("%s", p->name);
				printf("\n                      修改成功！\n"); break;
			case 3:
				printf("                     ☆—————————————————————————————☆\n");
				printf("                      请输入要修改的电话：");
				scanf("%s", p->tel);
			case 4:
				printf("                     ☆—————————————————————————————☆\n");
				printf("                      请输入要修改的地址：");
				scanf("%s", p->adr);
				printf("\n                      修改成功！\n"); break;
			default:
				printf("\n                      请输入正确的指令!\n");
				system("pause");
				system("cls");
		  		break;
		    }
			if (c == 1 || c == 2 || c == 3 || c == 4)
				break;

		}
		else
			p = p->next;
	}
	if (flag == 1)
	{
		printf("\n                      没有找到要修改的记录！\n");
	}

}
void del(pointer *head)//删除客户信息
{
	int flag = 1;
	char id[20];
	pointer p, q;
	p = head;
	printf("                     ☆—————————————————————————————☆\n");
	printf("                           请输入要删除的客户的身份证号码：");
	scanf("%s", id);
	while (p != NULL&&flag)
	{
		if (strcmp(p->id, id) == 0)
		{
			if (p == *head)
			{
				*head = p->next;
				free(p);//释放空间
			}
			else
			{
				q->next = p->next;
				free(p);
			}
			flag = 0;
			count--;
		}
		else
		{
			q = p;
			p = p->next;
		}
	}
	if (flag == 1)
	{
		printf("\n                           没有找到要删除的客户信息！\n");
	}
	else
	{
		printf("                     ☆————                                          ————☆\n");
		printf("\n                           删除成功！\n");

	}


}
void search(pointer head)//查找客户信息
{
	int flag = 1;
	char id[20];
	pointer p;
	p = head;
	printf("\n            ☆——————————————————————————————————————————————★\n");
	printf("             请输入你要查找的身份证号码：");
	scanf("%s", &id);
	while (p!=NULL&&flag)
	{
		if (strcmp(p->id, id) == 0)//链表的查找
		{
			printf("\n             已查询到你要的信息：\n");
			printf("            ☆————                                                                            ————☆\n");
			flag = 0;
		}
		else
		{
			p = p->next;
		}
	}
	if (flag ==0)
	{

		printf("             编号    身份证号码            姓名        手机号码       地址                        预约时间\n");
		printf("\n             %-8d%-22s%-12s%-15s%-28s%d-%d-%d\n", p->sw, p->id, p->name, p->tel, p->adr, p->year, p->mou, p->day);
		printf("            ☆————                                                                            ————☆\n");
	}
	else
	{
		printf("\n             抱歉！您查询的身份证号码不存在\n");
	}

}
void show(pointer head)//浏览客户信息
{
	pointer p;
	p = head;
	if (p == NULL)
	{
		printf("\n\n                   ☆                   暂时没有客户预定口罩！                  ☆ \n");
	}
	else
	{
		printf("\n           ★————                                   客户信息                                      ————☆\n\n");
		printf("            编号    身份证号码            姓名        手机号码       地址                        预约时间\n\n");
	}
	while (p != NULL)
	{
		printf("            %-8d%-22s%-12s%-15s%-28s%4d年%2d月%2d日\n", p->sw, p->id, p->name, p->tel, p->adr, p->year, p->mou, p->day);
		p = p->next;
	}

}
int main()
{
	pointer head = fileread();//读取函数
	int choose = 0, choose1 = 0, choose2 = 0,mima=0,key=123456;
	while (1)
	{
		menu();
		scanf("%d", &choose);
		system("cls");
		switch(choose)
		{
		case 1:
			while (1)
			{
				menu_1();
				scanf("%d", &choose1);
				system("cls");
				if (choose1 == 1){ add(&head); filesave(head); }
				if (choose1 == 2)break;
				if (choose1 == 3)exit(0);
				if (choose1<1 || choose1>3)
				{
					printf("\n\n                   ☆                   非法输入，请重新选择！                  ☆ \n");
					system("pause");
				}
				system("cls");
			}break;
		case 2:
		    {
				  printf("\n\n                     ☆—————————————————————————————☆\n");
				  printf("                                    请输入管理员密码：");
				  scanf("%d", &mima);
				  if (key == mima)
				  {
					  while (1)
					  {
						  system("cls");
						  menu_2();
						  scanf("%d", &choose2);
						  system("cls");
						  if (choose2 == 1){ revise(head); filesave(head); }
						  if (choose2 == 2){ del(head); filesave(head); }
						  if (choose2 == 3){ search(head); }
						  if (choose2 == 4){ show(head); }
						  if (choose2 == 5)break;
						  if (choose2 == 0)exit(0);
						  if (choose<0 || choose>5)
						  {
							  printf("\n\n                   ☆                   非法输入，请重新选择！                  ☆ \n");
						  }
						  system("pause");
						  system("cls");
					  }break;
				  }
				  else
				  {
					  printf("\n                                    密码错误，请重新输入！\n");
				  }
				  system("pause");
				  system("cls");
		    }break;
		case 3:exit(0);
		default:
			printf("\n\n                   ☆                   非法输入，请重新选择！                  ☆ \n");
			system("pause");//使控制台不会一闪而过
			system("cls");//清除界面
		}
	}
	return 0;
}
