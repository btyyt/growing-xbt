import java.util.Random;
import java.util.Scanner;

/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-07-30
 * Time: 10:13
 */

//题目1
//    public class means {
//    public static int sum(int n) {
//        int count=0;
//        for(int i=0;i<=n;i++) {
//            if(i%10==9){
//                count++;
//            }
//            if(i/10==9) {
//                count++;
//            }
//        }
//        return count;
//    }
//    public static void main(String[] args) {
//        int n=100;
//        int s=sum(n);
//        System.out.println(s);
//    }
//}

//题目2
//   public class means {
//    public static void main(String[] args) {
//        int s=0;
//        int i=0;
//        for(i=1000;i<2000;i++) {
//            if((i%4==0&&i%100!=0)||(i%400==0)) {
//                System.out.println(i);
//            }
//        }
//    }
//}

//题目3
//    public class means {
//    public static void main(String[] args) {
//        int i=0;
//        for(i=2;i<100;i++) {
//            boolean s=fun(i);
//            if(s==true) {
//                System.out.println(i);
//            }
//        }
//    }
//    public static boolean fun(int n) {
//        for(int i=2;i<n/2;i++) {
//            if(n%i==0) {
//                return false;
//            }
//        }
//        return true;
//    }
//}

//题目4
//   public class means {
//    public static void main(String[] args) {
//        Scanner sc =new Scanner(System.in);
//        int n=sc.nextInt();
//        boolean s=fun(n);
//        if(s==true) {
//            System.out.println(n+"是素数");
//        } else {
//            System.out.println(n+"不是素数");
//        }
//    }
//        public static boolean fun(int n) {
//        for(int i=2;i<n/2;i++) {
//            if(n%i==0) {
//                return false;
//            }
//        }
//        return true;
//    }
//}

//题目5
//public class means {
//    public static void main(String[] args) {
//
//        Scanner sc =new Scanner(System.in);
//        int n=sc.nextInt();
//        if(n<18){
//            System.out.println("少年");
//        } else if(n>=19&&n<=28) {
//            System.out.println("青年");
//        } else if(n>=29&&n<=55){
//            System.out.println("中年");
//        } else {
//            System.out.println("老年");
//        }
//    }
//}

//题目6

//题目7
//public class means {
//    public static void main(String[] args) {
//        Scanner sc=new Scanner(System.in);
//        //电脑需要随机生成数字
//        Random random =new Random();
//        int rand =random.nextInt(100)+1;
//
//        while (true) {
//            System.out.println("请输入数字：");
//            int num = sc.nextInt();
//            if(num == rand) {
//                System.out.println("相等");
//                break;
//            }else if(num < rand) {
//                System.out.println("小了");
//            }else {
//                System.out.println("大了");
//            }
//        }
//    }
//}

//题目8
//public class means {
//    public static void main(String[] args) {
//        Scanner sc=new Scanner(System.in);
//        int n=sc.nextInt();
//        fun(n);
//    }
//    public static void fun(int n) {
//
//        for(int i = 100;i <= n;i++) {
//            int sum = 0;
//            int count = 0;
//            int tmp = i;
//            while (tmp != 0) {
//                count++;
//                tmp /= 10;
//            }
//            tmp = i;//123
//            while (tmp != 0) {
//                sum += Math.pow(tmp%10,count);
//                tmp = tmp/10;
//            }
//            if(sum == i) {
//                System.out.println(i);
//            }
//        }
//    }
//}

//题目9
//public class means {
//    public static void main(String[] args) {
//        double i=1;
//        double sum=0;
//        for(i=1;i<=100;i++) {
//            if(i%2==0) {
//                sum=sum-1/i;
//            } else {
//                sum=sum+1/i;
//            }
//        }
//        System.out.println(sum);
//    }
//}

//题目10
//public class means {
//    public static void main(String[] args) {
//        Scanner sc=new Scanner(System.in);
//        int n=sc.nextInt();
//        int m=sc.nextInt();
//        int s=fun(n,m);
//        System.out.println(s);
//    }
//    public static int fun(int n, int m){
//        int gcd = 1;
//        int k = 2;
//        while(k <= n && k <= m){
//            if(n % k == 0 && m % k == 0){
//                gcd = k;
//            }
//            k++;
//        }
//        return gcd;
//    }
//}
//题目11
//public class means{
//    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("请输入一个整数");
//        int num = scanner.nextInt();
//        int count = 0;
//        for(int i = 0;i < 32;i++) {
//            if(((num >> i) & 1) == 1) {
//                count++;
//            }
//        }
//        System.out.println(count);
//    }
//}

//题目12
//public class means {
//    public static void main(String[] args) {
//        Scanner sc=new Scanner(System.in);
//        System.out.println("请输入数字：");
//        int value=sc.nextInt();
//        System.out.println("偶数序列：");
//        for(int i=31;i>0;i-=2){
//            System.out.print((value>>i)&1);
//        }
//        System.out.println("");
//        System.out.println("奇数序列：");
//        for(int i=30;i>=0;i-=2){
//            System.out.print((value>>i)&1);
//        }
//    }
//}

//题目13
//public class means{
//    public static void main(String[] args) {
//        guessPassword();
//    }
//    public static void guessPassword() {
//        Scanner scanner =new Scanner(System.in);
//        int sum=3;
//        while (sum!=0) {
//            System.out.println("请输入你要猜的密码：");
//                    String password =scanner.nextLine();
//            if(password.equals("java")) {
//                System.out.println("登录成功！");
//                return ;
//            } else {
//                sum--;
//                System.out.println("你还有"+sum+"次机会");
//            }
//        }
//    }
//}

//题目14
//public class means {
//    public static void main(String[] args) {
//        Scanner sc=new Scanner(System.in);
//        int n=sc.nextInt();
//        fun(n);
//
//    }
//    public static void fun(int n) {
//        int i=0;
//        while(n!=0) {
//            if(n<0) {
//                n=-n;
//            }
//            System.out.println(n%10);
//            n/=10;
//        }
//    }
//}

//题目15
//public class means {
//    public static void main(String[] args) {
//        Scanner sc=new Scanner(System.in);
//        int n=sc.nextInt();
//        for(int i=1;i<n;i++) {
//            for(int j=1;j<=i;j++) {
//                System.out.print(i+"*"+j+"="+i*j+" ");
//            }
//            System.out.println();
//        }
//    }
//}