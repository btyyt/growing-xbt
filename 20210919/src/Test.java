import java.util.Scanner;

/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-09-19
 * Time: 10:49
 */
import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int i = 0;
        System.out.println("请输入四则运算式：");
        String a = sc.nextLine();//nextLine为字符串输入关键字
        //判断输入的字符串为简单的四则运算
        /**
         * 判断理念 正确的简单四则运算形式应该为 数字+符号+数字，那么，反之不为此形式者就不是四则运算。
         *
         *第一轮循环，判断前面的字符串是否为数字，找到第一个非数字字符，退出循环进行下一步判断。  i为定位字符串下标变量
         * 对找到第一个非数字字符进行判断，为四则运算符则进入下一轮循环。
         * 接下来剩余的字符串应全为数字，则只需设置判断数字的条件即可。
         */
        for (i=0; i<a.length(); i++) {
            if(i==0&&(a.charAt(i)<'0'||a.charAt(i)>'9')) {			//第一个字符必须为数字，否则不为四则运算，即提示并退出
                System.out.println("不是正确的四则运算式子！！！");
                System.exit(0);
            }
            if(a.charAt(i)<'0'||a.charAt(i)>'9')
                break;
        }


        char b;
        int x;
        x = i;
        b = a.charAt(i);


        if(b=='+'||b=='-'||b=='*'||b=='/') {			//运算符判断
            i++;										//位置下标后移
            for(;i<a.length();i++) {
                if(a.charAt(i)<'0'||a.charAt(i)>'9') {
                    System.out.println("不是正确的四则运算式子！！！");
                    System.exit(0);
                }
            }
        }
        else {
            System.out.println("不是正确的四则运算式子！！！");
            System.exit(0);
        }

        //分离字符 将数字和符号分离x,提取字符串中的数字
        String c = a.substring(0, x);
        String d = a.substring(x+1, a.length());

        //此处提取出的数字为字符串型，所以需要对其转化
        //强制类型转化，将string型转化成int
        double e = Integer.parseInt(c);
        double f = Integer.parseInt(d);

        double s=0;


        //使用switch语句 寻找到其对应的运算规则，完成运算。此处也可以使用if-else
        switch(b) {
            case '+':
                s = e+f;
                System.out.println(e+"+"+f+"="+s);
                break;
            case '-':
                s = e-f;
                System.out.println(e+"-"+f+"="+s);
                break;
            case '*':
                s = e*f;
                System.out.println(e+"*"+f+"="+s);
                break;
            case '/':
                if(f==0) {
                    System.out.println("除数不能为零！！！");
                    break;
                }
                else {
                    s = e/f;
                    System.out.println(e+"/"+f+"="+s);
                    break;
                }
            default:
                System.out.println("不正确的四则运算式子！");

        }

        sc.close();			//代表关闭输入流，不然会有警告
    }

}
//public class Test {
//    public static void main(String[] args) {
//        Scanner sc=new Scanner(System.in);
//        System.out.println("请输入数字：");
//        int value=sc.nextInt();
//        System.out.println("偶数序列：");
//        for(int i=31;i>0;i-=2){
//            System.out.print((value>>i)&1);
//        }
//        System.out.println("");
//        System.out.println("奇数序列：");
//        for(int i=30;i>=0;i-=2){
//            System.out.print((value>>i)&1);
//        }
//    }
//}

//public class Test {
//    public static void main(String[] args) {
//        int n=10,m=20;
//        int max=max2(n,m);
//        System.out.println("两个整数的最大值为："+max);
//        double i=35.2,j=35.9;
//        double sum=max3(i,j);
//        System.out.println("两个小数的最大值为："+sum);
//        double max4=fix(n,m,i);
//        System.out.println("两个小数和一个整数的最大值为：" +max4);
//    }
//    public static int max2(int n,int m) {
//        if(n>m) {
//            return n;
//        } else {
//            return m;
//        }
//    }
//    public static double max3(double i,double j) {
//        if(i>j) {
//            return i;
//        } else {
//            return j;
//        }
//    }
//    public static double fix(double i,double j,int n) {
//        if(n>max3(i,j)) {
//            return n;
//        } else {
//            return max3(i,j);
//        }
//    }
//}

//求最大值
//public class Test {
//    public static void main(String[] args) {
//        int n=10,m=20;
//        int max=max2(n,m);
//        System.out.println("两个数的最大值为："+max);
//        int i=20,j=30,t=40;
//        int max1=max3(i,j,t);
//        System.out.println("三个数的最大值为："+max1);
//    }
//    public static int max2(int n,int m) {
//        if(n>m) {
//            return n;
//        } else {
//            return m;
//        }
//    }
//    public static int max3(int i,int j,int t) {
//        if(t>max2(i,j)) {
//            return t;
//        } else {
//            return max2(i,j);
//        }
//    }
//}

//递归实现斐波那契数列
//public class Test {
//    public static int fib (int n) {
//        if(n == 1||n == 2) {
//            return 1;
//        }
//        return fib(n - 1)+fib(n - 2);
//    }
//    public static void main(String[] args) {
//        Scanner sc = new Scanner(System.in);
//        int n = sc.nextInt();
//        int m = fib(n);
//        System.out.println(m);
//    }
//}

//迭代实现斐波那契数列的第n项
//迭代法也称辗转法
//public class Test {
//    public static void main(String[] args) {
//        Scanner sc = new Scanner(System.in);
//        int n=sc.nextInt();
//        int sum=fib(n);
//        System.out.println("sum="+sum);
//    }
//    public static int fib(int n) {
//        if(n == 1 || n == 2) {
//            return 1;
//        }
//        int lasd1=1,lasd2=1;
//        int sum=0;
//        for(int i=3; i<=n; i++) {
//            sum=lasd1+lasd2;
//            lasd2=lasd1;
//            lasd1=sum;
//        }
//        return sum;
//    }
//}