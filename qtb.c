#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>//静态表的查找操作
#define OK 1
#define MAXSIZE 10
typedef int status;

typedef int ElemType;
typedef struct
{
	ElemType *R;
	int length;
}SSTable;

status InitList(SSTable &ST)
{
	ST.R = new ElemType[MAXSIZE + 1];
	if (!ST.R)
		exit(0);
	ST.length = 0;
	return OK;
}

void CreatSSTable(SSTable &ST, int n)
{
	int i;
	printf("输入查找表的元素（%d个）：", n);
	for (i = 1; i <= n; i++)
	{
		scanf("%d", &ST.R[i]);
	}
	ST.length = n;
}
status Search_Seq(SSTable &ST, ElemType key)
{
	int i;
	ST.R[0] = key;
	for (i = ST.length; ST.R[i] != key; i--);
	return i;
}

int Search_Bin(SSTable ST, ElemType key)
{
	int low = 1;
	int high = ST.length;
	int mid;
	while (low <= high)
	{
		mid = (low + high) / 2;
		if (key == ST.R[mid])return mid;
		else if (key < ST.R[mid]) high = mid - 1;
		else low = mid + 1;
	}
	return 0;
}
int main()
{
	int m, n, low = 1;
	int high, key1, key2;
	SSTable L1;
	SSTable L2;
	InitList(L1);
	InitList(L2);

	printf("顺序查找\n");
	printf("请输入顺序表的长度：");
	scanf("%d", &m);
	CreatSSTable(L1, m);
	printf("请输入需要查找的元素：");
	scanf("%d", &key1);
	printf("%d在顺序表中的位置为：%d\n", key1, Search_Seq(L1, key1));
	printf("\n");
	printf("折半查找\n");
	printf("请输入顺序表的长度：");
	scanf("%d", &n);
	CreatSSTable(L2, n);
	printf("请输入需要查找的元素：");
	scanf("%d", &key2);
	printf("%d在顺序表中的位置为：%d\n", key2, Search_Bin(L2, key2));
	printf("\n");
	return 0;

}