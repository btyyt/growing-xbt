import java.util.Arrays;

/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-11-01
 * Time: 11:17
 */

public class TestDemo {
    public static int partition(int[] array,int start,int end) {
        int tmp = array[start];
        while (start < end) {
            //1、先判断后面
            while (start < end && array[end] >= tmp) {
                end--;
            }
            array[start] = array[end];
            //1.1 后面的给start  array[start] = array[end]
            //2、再判断前边
            while (start < end && array[start] <= tmp) {//
                start++;
            }
            array[end] = array[start];
            //2.1 把这个大的给end  array[end] = array[start]
        }
        array[start] = tmp;
        return tmp;
    }
    public static void medianOfThree(int[] array,int left,int right) {
        int mid = (left+right)/2;
        if(array[mid] > array[left]) {
            int tmp = array[mid];
            array[mid] = array[left];
            array[left] = tmp;
        }//array[mid] <= array[start]
        if(array[left] > array[right]) {
            int tmp = array[left];
            array[left] = array[right];
            array[right] = tmp;
        }//array[start] <= array[right]
        if(array[mid] > array[right]) {
            int tmp = array[mid];
            array[mid] = array[right];
            array[right] = tmp;
        }
        //array[mid] <= array[start] <= array[right]
    }



    public static void insertSort2(int[] array,int left,int right) {
        //参数判断
        if(array == null) return;
        for (int i = left+1; i <= right; i++) {
            int tmp = array[i];
            int j = i-1;
            for (; j >= left ; j--) {
                if(array[j] > tmp) {
                    array[j+1] = array[j];
                }else {
                    break;
                }
            }
            array[j+1] = tmp;
        }
    }
    public static void quick(int[] array,int left,int right) {
        if(left >= right) {
            return;
        }
        //3、递归执行到一个区间之后  进行直接插入排序
        if((right - left + 1) <= 100) {
            insertSort2(array,left,right);
            return;//
        }

        //1、随机选择基准-》先将left下标的值换一下
        //rand  交换array[left]  array[rand]
        //Random random = new Random();
        //int rand = random.nextInt(right-left)+left+1;


        //2、三数取中法
        medianOfThree(array,left,right);
        int pivot = partition(array,left,right);
        quick(array,left,pivot-1);
        quick(array,pivot+1,right);
    }
    public static void quickSort(int[] array) {
        quick(array,0,array.length-1);
    }

    public static void main(String[] args) {
        int[] array={6,1,7,9,3,4,5,10,8};
        System.out.println(Arrays.toString(array));
        quickSort(array);
        //quick(array,0,array.length);
        System.out.println(Arrays.toString(array));
    }
}
