import java.util.*;

/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-10-15
 * Time: 17:20
 */
public class Test {
    public static void main(String[] args) {

    }
    public static void main1(String[] args) {//集合栈
        Stack<Integer> stack = new Stack<>();
        stack.push(1);
        stack.push(2);
        stack.push(3);//添加栈元素
        System.out.println(stack.peek());//获取栈顶元素,不删除
        System.out.println(stack.pop());//弹出栈顶元素
        System.out.println(stack.isEmpty());//判断栈是否满了
        System.out.println(stack.size());//栈的元素数量
    }
}

//public class Test {
//    public static void main(String[] args) {
//        List<String> courses = new ArrayList<>();//集合数组
//        courses.add("C语言");
//        courses.add("Java SE");
//        courses.add("Java web");
//        courses.add("Java EE");
//        courses.add("C语言");
//        System.out.println(courses);
//        System.out.println("============");
//        System.out.println(courses.get(0));
//        System.out.println("============");
//        System.out.println(courses);
//        System.out.println("============");
//        courses.set(0,"计算机基础");
//        System.out.println(courses);
//        System.out.println("============");
//        List<String> subCourses = courses.subList(1,3);
//        System.out.println(subCourses);
//        System.out.println("============");
//        List<String> courses2 = new ArrayList<>(courses);//顺序表
//        System.out.println(courses2);
//        System.out.println("============");
//        List<String> courses3 = new LinkedList<>(courses);//双向链表
//        System.out.println(courses3);
//        System.out.println("============");
//        ArrayList<String> courese4 = (ArrayList<String>) courses2;
//        System.out.println(courese4);
//        System.out.println("============");
//        LinkedList<String> courses5 = (LinkedList<String>) courses3;
//        System.out.println(courses5);
//        System.out.println("============");
//    }
//}
//class Student {
//    private String name;
//    private String classes;
//    private double score;
//
//    public Student(String name, String classes, double score) {
//        this.name = name;
//        this.classes = classes;
//        this.score = score;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getClasses() {
//        return classes;
//    }
//
//    public void setClasses(String classes) {
//        this.classes = classes;
//    }
//
//    public double getScore() {
//        return score;
//    }
//
//    public void setScore(double score) {
//        this.score = score;
//    }
//
//    @Override
//    public String toString() {
//        return "Student{" +
//                "name='" + name + '\'' +
//                ", classes='" + classes + '\'' +
//                ", score=" + score +
//                '}';
//    }
//}
//
//public class Test {
//    public static void func(String str1, String str2) {
//        if (str1 == null || str2 ==null) {
//            return;
//        }
//        //List<Character> list = new ArrayList<>();
//        char[] list = new char[str1.length()] ;
//        int j= 0;
//        for (int i = 0; i < str1.length(); i++) {
//            char ch = str1.charAt(i);
//            if (!str2.contains(ch+"")) {
//                list[j] = ch;
//                j++;
//            }
//        }
//        for (int k = 0 ;k < j; k++) {
//            System.out.print(list[k]);
//        }
//
//    }
//    public static void main(String[] args) {
//        func("welcome to bit","come");
//    }



//    public static void main(String[] args) {
//        List<Integer> list = new ArrayList<>();
//        list.add(10);
//        list.add(100);
//        list.add(20);
//        Collections.sort(list);
//        System.out.println(list);
//    }
//    public static void main(String[] args) {
//        List<Student> list1 = new ArrayList<>();
//        list1.add(new Student("bit","5ban",99));
//        list1.add(new Student("bit","5ban",99));
//        System.out.println(list1);
//    }
//}
