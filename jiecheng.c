#define _CRT_SECURE_NO_WARNINGS 1//递归

#include<stdio.h>

int tg(int n)

{

  if(n<=1)

    return 1;

  else

    return n*tg(n-1);

}

int main()

{

  int n;

  scanf("%d",&n);

  int sum=tg(n);

  printf("%d",sum);

  return 0;

}

#define _CRT_SECURE_NO_WARNINGS 1//非递归

#include<stdio.h>

int main()

{

  int n;

  int sum=1,i;

  scanf("%d",&n);

  for(i=1;i<=n;i++)

  {

    sum=sum*i;

  }

  printf("%d",sum);

  return 0;

}
//递归和非递归分别实现求n的阶乘（不考虑溢出的问题）