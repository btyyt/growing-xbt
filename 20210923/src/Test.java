import java.util.Scanner;

/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-09-23
 * Time: 13:11
 */
public class Test{

    public static void main(String[] args){
        String s1="abc"+"def";//1
        String s2=new String(s1);//2
        if(s1.equals(s2))//3
            System.out.println(".equals succeeded");//4
        if(s1==s2)//5
            System.out.println("==succeeded");//6
    }
}