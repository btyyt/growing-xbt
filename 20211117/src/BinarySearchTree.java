
/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-11-17
 * Time: 15:02
 */

public class BinarySearchTree {

    static class TreeNode {
        public int val;
        public TreeNode left;
        public TreeNode right;

        public TreeNode(int val) {
            this.val = val;
        }
    }

    public TreeNode root;

    //搜索操作
    public TreeNode search(int key) {
        TreeNode cur = root;
        while(cur != null) {
            if(cur.val == key) {
                return cur;
            } else if(cur.val < key) {
                cur = cur.right;
            }else  {
                cur = cur.left;
            }
        }
        return null;
    }


    //插入操作
    public void insertTree(int val) {
        if(root == null) {
            root = new TreeNode(val);
            return;
        }
        TreeNode cur = root;
        TreeNode parent = null;

        while(cur != null) {
             if(cur.val < val) {
                parent = cur;
                cur = cur.right;
            } else {
                parent = cur;
                cur = cur.left;
            }
        }
        TreeNode node = new TreeNode(val);
        if(parent.val < val) {
            parent.right = node;
        } else {
            parent.left = node;
        }
    }
    //删除操作
    public void remove(int key) {
        TreeNode cur = root;
        TreeNode parent = null;
        while(cur != null) {
            if(cur.val == key) {
                removeNode(parent,cur);
                return ;
            } else if(cur.val < key) {
                parent = cur;
                cur = cur.right;
            } else {
                parent = cur;
                cur = cur.left;
            }
        }
    }
    public void removeNode(TreeNode parent,TreeNode cur) {
        if(cur.left == null) {
            if(cur == root) {
                root = cur.right;
            } else if(cur == parent.left) {
                parent.left = cur.right;
            } else {
                parent.right = cur.right;
            }
        } else if(cur.right == null) {
            if(cur == root) {
                root = cur.left;
            } else if(cur == parent.left) {
                parent.left = cur.left;
            } else {
                parent.right = parent.left;
            }
        } else {//替罪羊的删除*//左边找最大，右边找最小
            TreeNode targetparent = cur;
            TreeNode target = cur.right;
            while(target.left != null) {
                targetparent = target;
                target = target.left;
            }
            cur.val = target.val;
            if(targetparent.left == target) {
                targetparent.left = target.right;
            } else {
                targetparent.right = target.right;
            }
        }
    }

    //遍历二叉搜索树
//    public void inorder(TreeNode root) {
//        if(root == null) {
//            return;
//        }
//        inorder(root.left);
//        System.out.print(root.val+" ");
//        inorder(root.right);
//    }
    public void preOrder(TreeNode root) {
        if(root == null) return;
        System.out.print(root.val+" ");
        preOrder(root.left);
        preOrder(root.right);
    }

}

