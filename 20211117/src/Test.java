/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-11-17
 * Time: 14:31
 */

public class Test {
    public static void main(String[] args) {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
        binarySearchTree.insertTree(10);
        binarySearchTree.insertTree(5);
        binarySearchTree.insertTree(7);
        binarySearchTree.insertTree(3);
        binarySearchTree.insertTree(2);
        binarySearchTree.preOrder(binarySearchTree.root);
        binarySearchTree.remove(5);
        System.out.println("==============");
        binarySearchTree.preOrder(binarySearchTree.root);
        System.out.println("==============");
        binarySearchTree.remove(10);
        binarySearchTree.preOrder(binarySearchTree.root);

    }
}
