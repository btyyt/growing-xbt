import java.util.Random;
import java.util.Scanner;
import java.util.Arrays;

public class control {
    public static int fun(int n) {
        int sum=1;
        for(int i=1;i<=n;i++) {
            sum*=i;
        }
        return sum;
    }
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int sum=fun(n);
        System.out.println(sum);
    }
}

//public class control {
//    public static int fun(int sum) {
//        int s=1;
//        int m=0;
//        for(int i=1;i<=sum; i++) {
//            for(int j=1;j<=i;j++) {
//                s*=j;
//            }
//            m=m+s;
//            s=1;
//        }
//        return m;
//    }
//    public static void main(String[] args) {
//        Scanner sc =new Scanner(System.in);
//        int sum=sc.nextInt();
//        int m=fun(sum);
//        System.out.println(m);
//    }
//}

//public class control {
//    public static int find(int[] arr) {
//        for(int i=0;i<arr.length;i++) {
//            int sum=0;
//            for(int j=0;j<arr.length;j++) {
//                if(arr[j]==arr[i]) {
//                    sum=sum+1;
//                }
//            }
//            if(sum==1) {
//                return i;
//            }
//        }
//        return 0;
//    }
//    public static void main(String[] args) {
//        int[] arr={1,2,3,2,1};
//        int sum=find(arr);
//        System.out.println(arr[sum]);
//    }
//}

//public class control {
//    public static void main(String[] args) {
//        int[] array={1,2,3,4};
//        transform(array);
//        System.out.println(Arrays.toString(array));
//    }
//    public static void transform(int[] array) {
//        int left=0;
//        int right=array.length-1;
//        while(left<right) {
//            while(left<right&&array[left]%2==0) {
//                left++;
//            }
//            while(left<right&&array[right]%2!=0) {
//                right--;
//            }
//            int tmp=array[left];
//            array[left]=array[right];
//            array[right]=tmp;
//        }
//    }
//}