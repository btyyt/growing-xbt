#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
void print(int a[])//打印数组的元素
{
	int i = 0;
	for (i = 0; i < 10; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}
void init(int a[])//初始化数组为0
{
	int i = 0;
	for (i = 0; i < 10; i++)
	{
		a[i] = 0;
	}
}
void reverse(int a[])//完成数组元素的逆置
{
	int left = 0;
	int right= 9 ;
	int t;
	while (left < right)
	{
		t = a[left];
		a[left] = a[right];
		left++;
		right--;
	}
}
int main()
{
	int a[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	print(a);
	reverse(a);
	print(a);
	init(a);
	print(a);
	return 0;
}