import java.util.Scanner;

/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-08-17
 * Time: 23:32
 */

//public class Target {
//    public static int addInt(int a,int b) {
//        int c=a+b;
//        return c;
//    }
//    public static double addDouble(double a1,double b1) {
//        double c1=a1+b1;
//        return c1;
//    }
//    public static void main(String[] args) {
//        int a=100;
//        int b=100;
//        int c=addInt(a,b);
//        System.out.println("c="+c);
//
//        double a1=100;
//        double b1=100;
//        double c1=addDouble(a1,b1);
//        System.out.println("c1="+c1);
//    }
//}


//求斐波那契数列的第n项。(迭代实现)
//public class Target {
//    public static int fib(int b) {
//        if(b<=2) {
//            return 1;
//        }
//        return (fib(b-1)+fib(b-2));
//    }
//    public static void main(String[] args) {
//        Scanner sc=new Scanner(System.in);
//        int a=sc.nextInt();
//        int b=fib(a);
//        System.out.println("b="+b);
//    }
//}

//Justin Bieber
//public class Target {
//    public static void main(String[] args) {
//        System.out.println("Justin Bieber");
//        System.out.println("Love yourself");
//        System.out.println("Love me");
//        System.out.println("Mistletoe");
//    }
//}

//递归求 1 + 2 + 3 + ... + 10
//public class Target {
//    public static int add(int n) {
//        if(n==1) {
//            return 1;
//        }
//        return n+add(n-1);
//    }
//    public static void main(String[] args) {
//        Scanner sc=new Scanner(System.in);
//        int n=10;//10
//        int sum=add(n);
//        System.out.println("sum="+sum);
//    }
//}

//递归求n的阶乘
//public class Target {
//    public static int fun(int n) {
//        if(n==1) {
//            return 1;
//        }
//        return n*fun(n-1);
//    }
//    public static void main(String[] args) {
//        Scanner sc=new Scanner(System.in);
//        int n=0;
//        n=sc.nextInt();
//        int sum=fun(n);
//        System.out.println("sum="+sum);
//    }
//}

