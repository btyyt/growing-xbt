/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-11-15
 * Time: 19:20
 */
class Car {
    protected String model;
    protected int speed;
    public Car() {

    }
    public Car(String model, int speed) {
        this.model = model;
        this.speed = speed;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", speed=" + speed +
                '}';
    }
}
interface Bluetooth {
    void bluetooth();
}
interface GPS {
    void gps();
}
interface IOV {
    void iov();
}
class miCar extends Car {
    public miCar(String model, int speed) {
        super(model,speed);
    }

    @Override
    public String toString() {
        return "miCar{" +
                "model='" + model + '\'' +
                ", speed=" + speed +
                '}';
    }

}
class MiddleCar extends Car implements Bluetooth {
    public MiddleCar(String model, int speed) {
        super(model,speed);
    }

    @Override
    public String toString() {
        return "MiddleCar{" +
                "model='" + model + '\'' +
                ", speed=" + speed +
                '}';
    }

    @Override
    public void bluetooth() {
        System.out.println(this.model+"正在启动关闭蓝牙功能");
    }
}
class LuxuryCar extends Car implements Bluetooth, GPS, IOV {
    public LuxuryCar(String model, int speed) {
        super(model, speed);
    }
    @Override
    public String toString() {
        return "LuxuryCar{" +
                "model='" + model + '\'' +
                ", speed=" + speed +
                '}';
    }

    @Override
    public void bluetooth() {
        System.out.println(this.model+"正在启动关闭蓝牙功能");
    }

    @Override
    public void gps() {
        System.out.println(this.model+"正在提供GPS定位功能");
    }

    @Override
    public void iov() {
        System.out.println(this.model+"正在进行车联网。");
    }
}
public class TestDemo {
    public static void main(String[] args) {
        miCar miCar = new miCar("小米汽车",66);
        System.out.println(miCar.toString());
        MiddleCar middleCar = new MiddleCar("宝马5s",100);
        System.out.println(middleCar.toString());
        middleCar.bluetooth();
        LuxuryCar luxuryCar = new LuxuryCar("劳斯莱斯",120);
        System.out.println(luxuryCar.toString());
        luxuryCar.bluetooth();
        luxuryCar.gps();
        luxuryCar.iov();
    }

}
