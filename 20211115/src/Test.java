/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-11-15
 * Time: 21:04
 */


class BinarySearchTree {

    static class TreeNode {
        public int val;
        public TreeNode left;
        public TreeNode right;

        public TreeNode(int val) {
            this.val = val;
        }
    }

    public TreeNode root;

    //搜索操作
    public TreeNode search(int key) {
        TreeNode node = root;
        while(node != null) {
            if(node.val == key) {
                return node;
            } else if(key < node.val) {
                node = node.left;
            }else if(key > node.val) {
                node = node.right;
            }
        }
        return null;
    }
    //插入操作
    public boolean insertTree(int val) {
        if(root == null) {
            root = new TreeNode(val);
            return true;
        }
        TreeNode cur = root;
        TreeNode parent = null;

        while(cur != null) {
            if(cur.val == val) {
                return false;
            } else if(cur.val < val) {
                parent = cur;
                cur = cur.right;
            } else {
                parent = cur;
                cur = cur.left;
            }
        }
        TreeNode node = new TreeNode(val);
        if(parent.val < val) {
            parent.right = node;
        } else {
            parent.left = node;
        }
    }
    //删除操作
}
public class Test {
    public static void main(String[] args) {
        BinarySearchTree binarySearchTree = new BinarySearchTree(10);

    }
}
