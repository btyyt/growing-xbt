//public class Car {
//    //1. 定义成员变量
//    private String brand;
//    private double speed;
//    //2. 定义构造方法
//    public Car() {}
//    public Car(String brand, double speed) {
//        this.brand = brand;
//        this.speed = speed;
//    }
//    //3. 必要的setter和getter方法
//    public double getSpeed() {
//        return this.speed;
//    }
//    public String getBrand() {
//        return this.brand;
//    }
//    //4. 功能方法
//    public String toString() {
//        StringBuilder sb=new StringBuilder();
//        sb.append("品牌：").append(this.brand);
//        sb.append("，速度：").append(this.speed);
//        return sb.toString();
//    }
//}
//
////定义蓝牙接口
//public interface Bluetooth {
//    public void  startBluetooth();//启动蓝牙
//    public void closeBluetooth();//关闭蓝牙
//}
////定义GPS接口
//public interface GPS {
//    public Point getLocation();//获得位置
//}
////定义IOV接口
//public interface IOV {
//    public void getConnetion(Car car);//与其它汽车建立IOV关系
//}
//
////定义中配汽车
//public class MiddleCar extends Car implements Bluetooth{
//    //1. 定义成员变量
//    private boolean bluetoothState=false;//蓝牙状态
//    //2. 定义构造方法
//    public MiddleCar() {	}
//    public MiddleCar(String brand, double speed) {
//        super(brand, speed);
//    }
//    //3. 定义setter和getter方法
//    public boolean getBluetoothState() {
//        return this.bluetoothState;
//    }
//    //4. 定义功能方法
//    @Override
//    public void startBluetooth() {//启动蓝牙
//        if(this.bluetoothState==false) {
//            this.bluetoothState=true;
//        }
//    }
//    @Override
//    public void  closeBluetooth() {//关闭蓝牙
//        if(this.bluetoothState==true) {
//            this.bluetoothState=false;
//        }
//    }
//    public String toString() {
//        StringBuilder sb=new StringBuilder();
//        sb.append(super.toString());
//        sb.append("，蓝牙状态：").append(this.bluetoothState);
//        return sb.toString();
//    }
//}
////定义高配汽车
//public class LuxuryCar extends MiddleCar implements GPS,IOV{
//    //1. 定义成员变量
//    private Point location;//位置
//    //2. 定义构造方法
//    public LuxuryCar() {
//        super();
//        // TODO Auto-generated constructor stub
//    }
//    public LuxuryCar(String brand, double speed) {
//        super(brand, speed);
//        // TODO Auto-generated constructor stub
//    }
//    //3. 定义setter和getter方法
//    //4. 定义功能方法
//    @Override
//    public void getConnetion(Car car) {
//        if(car instanceof IOV) {//如果该汽车是IOV的子类实例，表明具有IOV功能
//            System.out.println(this.getBrand()+"建立与"+car.getBrand()+"的连接！");
//        }
//        else {
//            System.out.println(car.getBrand()+"没有物联网接口！");
//            System.out.println(this.getBrand()+"不能建立与"+car.getBrand()+"的连接！");
//        }
//    }
//    @Override
//    public Point getLocation() {
//        Point p=new Point();
//        double x=new Random().nextInt(50);
//        double y=new Random().nextInt(50);
//        p.setLocation(this.getSpeed()+x, this.getSpeed()+y);
//        return p;
//    }
//    public String toString() {
//        StringBuilder sb=new StringBuilder();
//        sb.append(super.toString());
//        sb.append(",位置：").append(this.getLocation().getX());
//        sb.append(","+this.getLocation().getY());
//        return sb.toString();
//    }
//}
////定义测试类
//public class Demo9 {
//    public static void main(String[] args) {
//        System.out.println("------- 测试低配汽车  -------------");
//        Car gen=new Car("红旗L5",60);
//        System.out.println(gen);
//        System.out.println("------- 测试中配汽车  -------------");
//        MiddleCar mc=new MiddleCar("红旗L5",80);
//        mc.closeBluetooth();
//        System.out.println(mc);
//        mc.startBluetooth();
//        System.out.println(mc);
//        System.out.println("------- 测试高配汽车  -------------");
//        LuxuryCar lc1=new LuxuryCar("红旗H5",120);
//        lc1.startBluetooth();
//        System.out.println(lc1);
//        lc1.getConnetion(mc);//红旗H5与红旗L5不能建立连接
//        LuxuryCar lc2=new LuxuryCar("宝马X5",100);
//        lc1.getConnetion(lc2);//红旗H5与宝马X5能建立连接
//    }
//}
