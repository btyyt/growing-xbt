#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <string.h>
#include <assert.h>
char* my_strstr(const char *a, const char *b)
{
	assert(a&&b);
	const char* s1 = NULL;
	const char* s2 = NULL;
	const char* cp = a;

	if (*b == '\0')
	{
		return (char*)a;
	}
	
	while (*cp)
	{
		s1 = cp;
		s2 = b;
		while (*s1 && *s2 && (*s1 == *s2))
		{
			s1++;
			s2++;
		}
		if (*s2 == '\0')
		{
			return (char*)cp;
		}
		cp++;
		}
	
	   return NULL;
}

int main()
{
	char a[] = "abcdefg";
	char b[] = "efgh";
	char *ret=my_strstr(a, b);
	if (ret == NULL)
	{
		printf("û�ҵ�\n");
	}
	else
	{
		printf("�ҵ���\n");
	}
	return 0;
}