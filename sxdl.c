#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#define MAX 100
typedef int SLDataType;
typedef int Status;
typedef struct
{
	SLDataType *base;
	int front;
	int rear;
}SqQueue;
//初始化一个空队列
Status InitQueue(SqQueue &q)
{
	q.base = (SLDataType*)malloc(MAX*sizeof(SLDataType));
	if (q.base == NULL)
		return 0;
	q.front = 0;
	q.rear = 0;
	return 1;
}
//求循环队列的长度
int QueueLenth(SqQueue q)
{
	return (q.rear - q.front + MAX) % MAX;
}
//将元素e入队
SLDataType EnQueue(SqQueue &q, SLDataType e)
{
	if ((q.rear + 1) % MAX == q.front)//队满
		return 0;
	q.base[q.rear] = e;
	q.rear = (q.rear + 1) % MAX;
	return 1;
}
//队头元素出队，并用e返回出队元素
Status DeQueue(SqQueue &q, SLDataType &e)
{
	if (q.front == q.rear)//对空
		return 0;
	e = q.base[q.front];//保存队头元素
	q.front = (q.front + 1) % MAX;
	return 1;
}
void PrintQueue(SqQueue q)
{
	int i;
	printf("队列中的元素有：");
	i = q.front;//从队头开始输出
	while (i<q.rear)
	{
		printf("%5d", q.base[i]);
		i++;
	}
	printf("\n");
}
int main()
{
	SqQueue q;
	SLDataType value;
	int n;
	if (InitQueue(q))
	{
		printf("队列初始化成功！\n");
		printf("队列的长度为：%d\n", QueueLenth(q));
		printf("对头：%d;队尾：%d\n", q.front, q.rear);
		//入队
		printf("输入需要入队的元素的个数：");
		scanf("%d", &n);
		for (int i = 0; i < n; i++)
		{
			printf("输入需要入队的元素：");
			scanf("%d", &value);
			if (EnQueue(q, value) == 1)
				printf("%d入队成功！\n", value);
			else
				printf("%d入队失败！\n", value);
		}
		printf("入队之后，队列的元素为：");
		PrintQueue(q);
		printf("队头: %d,队尾: %d\n", q.front, q.rear);
		printf("队列的长度：%d\n", QueueLenth(q));
		//出队
		if (DeQueue(q, value) == 1)
		{
			printf("队头元素出队成功！\n");
			printf("出队的元素为：%5d\n", value);
			printf("队头：%d;队尾：%d\n", q.front, q.rear);
			printf("队列的长度：%d\n", QueueLenth(q));
		}
		else
		{
			printf("队头元素出队失败！\n");
		}
	}
	return 0;
}
