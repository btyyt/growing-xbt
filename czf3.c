#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int main()
{   //&操作符
	int a = -10;
	printf("%d\n", !a);
	printf("%d\n", !2);
	printf("%d\n", !0);
	//&操作符
	int *p = NULL;
	a = -a;//a=10
	p = &a;
	printf("%p\n", p);
	//*解引用操作符
	printf("%d\n", *p);
	//sizeof操作符
	printf("%d\n", sizeof(int));
	printf("%d\n", sizeof(char));
	//(类型)强制类型转换
	printf("%d\n", sizeof(a));
	printf("%d\n", sizeof((char)a));
	return 0;
}
//
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int main()
{   
	//&&逻辑与
	int a = 10;
	if (a > 5 && a < 15)
	{
		printf("%d\n", a);
	}
	else
	{
		printf("不满足\n");
	}
	//逻辑或
	int b = 20;
	if (b>10 || b < 0)
	{
		printf("%d\n", b);
	}
	else
	{
		printf("不满足\n");
	}
	return 0;
}
