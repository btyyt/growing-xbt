#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
int main()
{
	int  a[] = { 1, 2, 3 };
	int  b[] = { 1, 5};
	int ret = memcmp(a,b, 8);//注意第3个参数的单位是字节
	if (ret > 0)
	{
		printf("a>b\n");
	}
	else if (ret < 0)
	{
		printf("a<b\n");
	}
	else
	{
		printf("a==b\n");
	}
	return 0;
}
