import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.sql.SQLOutput;

/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-10-13
 * Time: 10:00
 */

//class Person {
//
//}
//public class Test {
//    public static void main(String[] args) {
//
//    }
//}
//public class Animal {
//    protected String name;
//    public Animal(String name) {
//        this.name = name;
//    }
//    public void eat(String food) {
//        System.out.println(this.name + "正在吃" + food);
//    }
//}
//// Bird.java
//public class Bird extends Animal {
//    public Bird(String name) {
//        super(name);
//    }
//    public void fly() {
//        // 对于父类的 protected 字段, 子类可以正确访问
//        System.out.println(this.name + "正在飞 ︿(￣︶￣)︿");
//    }
//}
//// Test.java 和 Animal.java 不在同一个 包 之中了.
//public class Test {
//    public static void main(String[] args) {
//        Animal animal = new Animal("小动物");
//        System.out.println(animal.name); // 此时编译出错, 无法访问 name
//    }
//}

//class Person {
//    private String name;
//    private int age;
//    public Person(String name,int age) {
//        this.age = age;
//        this.name = name;
//    }
//    public void show() {
//        System.out.println("name:"+name+" "+"age:"+age);
//    }
//
//    @Override
//    public String toString() {
//        return "Person{" +
//                "name='" + name + '\'' +
//                ", age=" + age +
//                '}';
//    }
//}
//public class Test {
//    public static void main(String[] args) {
//        Person person = new Person("caocao",19);
//        person.show();
//        System.out.println(person);
//    }
//}
//class Person {
//    public int id;
//    public String name;
//    public String gender;
//    public int age;
//    public String area;
//    public Person(int id,String name,String gender,int age,String area) {
//        this.id = id;
//        this.name = name;
//        this.gender = gender;
//        this.age = age;
//        this.area = area;
//    }
//    public eating(String food) {
//
//    }
//    public void show( ) {
//        System.out.println("id: "+id+"name: "+name+"gender: "+gender+"age: "+age+"area: "+area);
//    }
//}
//public class Test {
//    public static void main(String[] args) {
//        Person person = new Person(100,"张帆","男",19,"张家界");
//        person.show();
//    }
//}



class Person {
    public int id;
    public String name;
    public double  javagrade;
    public double  sjgrade;
    public Person(int id,String name) {
        this.id = id;
        this.name = name;
    }
    public Person(int id,String name,double  javagrade,double  sjgrade) {
        this.id = id;
        this.name = name;
        this.javagrade = javagrade;
        this.sjgrade = sjgrade;
    }
    public double  Sumgrade(Person person) {
        double  sumgrade = person.javagrade + person.sjgrade;
        return sumgrade;
    }
    public double  Grade(Person person) {
        double grade = (person.javagrade +person.sjgrade)/2;
        return grade;
    }
    public void Mook(String time,String book,String area) {
        System.out.println("上课时间: "+time+"课程名称："+book+"上课地点: "+area);
    }
    public void show() {
        System.out.println("id: "+id+"name: "+name+"javagrade: "+javagrade+"sjgrade: "+sjgrade);
    }

}
public class Test {
    public static void main(String[] args) {
        Person person1 = new Person(2021,"张帆",90,85);
        person1.show();
        System.out.println("总分: "+ person1.Sumgrade(person1) +"平均分: "+ person1.Grade(person1));
        person1.Mook("2017年9月29日", "Java课程","3223");
        System.out.println("===========================");
        Person person2 = new Person(2022,"李华",88,70);
        person2.show();
        System.out.println("总分: "+ person1.Sumgrade(person2) +"平均分: "+ person1.Grade(person2));
        person2.Mook("2017年9月29日","数据结构","1402");
    }
}
//class Person {
//    private int length;
//    private int  width;
//    public Person(int length,int width) {
//        this.length = length;
//        this.width = width;
//    }
//
//    public int getLength() {
//        return length;
//    }
//
//    public void setLength(int length) {
//        this.length = length;
//    }
//
//    public int getArea(int length, int width) {
//        return length*width;
//    }
//
//    public int getWidth() {
//        return width;
//    }
//
//    public void setWidth(int width) {
//        this.width = width;
//    }
//
//    public int getPerimeter(int length, int width) {
//        return 2*(length+width);
//    }
//
//    @Override
//    public String toString() {
//        return "Person{" +
//                "length=" + length +
//                ", width=" + width +
//                '}';
//    }
//}
//public class Test {
//    public static void main(String[] args) {
//        Person person = new Person(20,40);
//        int area = person.getArea(person.getLength(), person.getWidth());
//        System.out.println("面积为： "+area);
//        int perimeter = person.getPerimeter(person.getLength(), person.getWidth());
//        System.out.println("周长为： "+perimeter);
//        System.out.println(person);
//    }
//}
