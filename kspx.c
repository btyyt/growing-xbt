#include<stdio.h>//快速排序
int a[100];//这里将数组a定义为全局变量，方便后面使用
void kspx(int left,int right)
{
    int i,j;
    int t,bjs;//bjs就是指开头的比较数
    if(left>right)
        return;
    bjs=a[left];
    i=left;
    j=right;
    while(i!=j)
    {
        while (a[j]>=bjs&&i<j)//这里是从右往左走
            j--;
        while(a[i]<=bjs&&i<j)//这里是从左往右走
            i++;
        if(i<j)//当i,j还没有相遇的时候
        {
            t=a[i];
            a[i]=a[j];
            a[j]=t;
        }
    }
    a[left]=a[i];//将比较数换到i,j相遇的位置
    a[i]=bjs;
    kspx(left,i-1);//下面使用递归进行下面的排序
    kspx(i+1,right);//使其排好
}
int main()
{
    int i,j;
    int n;
    scanf("%d",&n);//首序列长度
    for(i=1;i<=n;i++)
    scanf("%d",&a[i]);
    kspx(1,n);//快速排序函数
    for(i=1;i<=n;i++)//验证结果
        printf("%d ",a[i]);
    return 0;
}

