public class Test {
    public static int[] copyArray(int[]src,int begin,int end,int[]dest) {//2 3 4 5 6(2~6)
        int[] test={0,0,0,0,0};
        for (int i=0; i <=begin; i++) {
            if(i == begin) {
                int s=0;
                for(int j=begin ; j <= end; j++) {
                    int t=src[j];
                    test[s]=t;
                    s++;
                }
                dest = test;
                return dest;
            }
        }
        return null;
    }
    public static int getMax(int[] a) {
        int max=0;
        for (int i=0; i<a.length; i++) {
            if(a[i] > max) {
                max = a[i];
            }
        }
        return max;
    }
    public static int getMax(int[][] b) {
        int max = 0;
        for (int i=0; i<b.length; i++) {
            for (int j=0; j< b[i].length;j++) {
                if(b[i][j] > max) {
                    max = b[i][j];
                }
            }
        }
        return max;
    }
    public static double getMax(double d,double e,double f ) {
        double max = 0;
        if(d > e) {
             max = d;
        } else {
            max =e;
        }
        if(max > f) {
            return max;
        } else {
            return f;
        }
    }
    public static void main1(String[] args) {
        int[] src = {1,2,3,4,5,6,7};//2 3 4 5 6
        int[] dest = new int[5];
        int begin = 2,end = 6;//2 3 4 5 6
        dest = copyArray(src,begin,end,dest);
        for(int i=0;i<dest.length;i++) {
            System.out.print(dest[i]+" ");
        }
    }
    public static void main2(String[] args) {
        int[] a = {1,2,345,5,7};
        int maxa = getMax(a);
        System.out.println("一维数组最大值"+maxa);
        int[][] b={{1,34,6},{2,72,9}};
        int maxb = getMax(b);
        System.out.println("二维数组最大值"+maxb);
        double d = 178;
        double e = 435;
        double f = 12;
        double macd = getMax(d,e,f);
        System.out.println("三个浮点数中的最大值"+macd);
    }

    public static void main(String[] args) {//返回元素是浮点数的Vector中的最大元素
        Vector v = new Vector();

        v.add(new Double("3.4324"));

        v.add(new Double("3.3532"));

        v.add(new Double("3.342"));

        v.add(new Double("3.349"));

        v.add(new Double("2.3"));

        Object obj = Collections.max(v);

        System.out.println("最大元素是："+obj);
    }
}
