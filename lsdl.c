#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#define MAX 100
typedef int SLDataType;
typedef int Status;
typedef struct QNode
{
	SLDataType data;
	struct QNode *next;
}QNode, *QueuePtr;
typedef struct
{
	QueuePtr front;
	QueuePtr rear;
}LinkQueue;

//初始化一个空队列
Status InitQueue(LinkQueue &q)
{
	q.front = (QNode*)malloc(sizeof(QNode));
	if (q.front == NULL)
		return 0;
	q.rear = q.front;
	q.front->next = NULL;
	return 1;
}
int DestroyQueue(LinkQueue &p)
{
	while (p.front != NULL)
	{
		p.rear = p.front->next;
		free(p.front);
		p.front = p.rear;
	}
	return 1;
}
//判断队列是否为空
int QueueEmpty(LinkQueue p)
{
	return (p.front == p.rear);
}
//将元素e入队
SLDataType EnQueue(LinkQueue &q, SLDataType e)
{
	QNode *s = (QNode*)malloc(sizeof(QNode));
	if (s == NULL)
		return 0;
	s->data = e;
	s->next = NULL;
	q.rear->next = s;
	q.rear = s;
	return 1;
}
//队头元素出队，并用e返回出队元素
Status DeQueue(LinkQueue &q, SLDataType &e)
{
	QNode *s;
	if (q.front == q.rear)
		return 0;
	s = q.front->next;
	q.front->next = s->next;
	if (q.rear == s)
		q.rear = q.front;
	e = s->data;
	free(s);
	return 1;
}
//求队列的长度
int	 QueueLength(LinkQueue que)
{
	QNode *p;
	int i = 0;
	p = que.front->next;
	while (p != NULL)
	{
		i++;
		p = p->next;
	}
	return i;
}
//输出队列
void PrintQueue(LinkQueue q)
{
	QNode *p;
	printf("队列中的元素有：");
	p = q.front->next;
	while (p != NULL)
	{
		printf("%5d", p->data);
		p = p->next;
	}
	printf("\n");
}
int main()
{
	LinkQueue que;
	SLDataType value;
	int num, i;
	if (InitQueue(que))
	{
		printf("队列初始化成功！\n");
		printf("队列的长度为：%d\n", QueueLength(que));
		//入队
		printf("输入需要入队的元素的个数：");
		scanf("%d", &num);
		i = 1;
		printf("输入需要入队的元素：");
		while (i <= num)
		{
			scanf("%d", &value);
			if (EnQueue(que, value) == 1)
				printf("%d入队成功！\n", value);
			else
			{
				printf("%d入队失败！\n", value);
				break;
			}
			i++;
		}
		PrintQueue(que);
		printf("队列的长度：%d\n", QueueLength(que));
		//出队
		if (DeQueue(que, value) == 1)
		{
			printf("队头元素出队成功！\n");
			printf("出队的元素为：%5d\n", value);
			printf("队列的长度：%d\n", QueueLength(que));
			PrintQueue(que);
		}
		else
		{
			printf("队头元素出队失败！\n");
		}
	}
	return 0;
}
