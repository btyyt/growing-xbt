import java.util.Arrays;

/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-11-03
 * Time: 14:38
 */
class Solution {
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int[] tmp = new int[m+n];
        int s1=0;
        int e1=m-1;
        int s2=0;
        int e2=n-1;
        int k=0;
        while (s1 <= e1 && s2 <= e2) {
            if(nums1[s1] < nums2[s2]) {
                tmp[k++] = nums1[s1++];
            } else {
                tmp[k++] = nums2[s2++];
            }
        }
        while(s1 <= e1) {
            tmp[k++] = nums1[s1++];
        }
        while(s2 <= e2) {
            tmp[k++] = nums2[s2++];
        }
        for(int i=0; i < k; i++) {
            nums1[i] = tmp[i];
        }
    }
}
