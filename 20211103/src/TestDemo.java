import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * Created by YYT
 * Description:
 * User: YINYUNTAO
 * Date: 2021-11-03
 * Time: 14:08
 */
public class TestDemo {
    public static void merge(int[] array,int left,int right,int mid) {

        int[] tmp = new int[right-left+1];
        int k = 0;
        int s1 = left;
        int e1 = mid;

        int s2 = mid+1;
        int e2 = right;

        while (s1 <= e1 && s2 <= e2) {
            if(array[s1] <= array[s2]) {
                tmp[k++] = array[s1++];
            }else {
                tmp[k++] = array[s2++];
            }
        }
        while (s1 <= e1) {
            tmp[k++] = array[s1++];
        }
        while (s2 <= e2) {
            tmp[k++] = array[s2++];
        }
        for (int i = 0; i < k; i++) {
            //排好序的数字 放到原数组合适的位置
            array[i+left] = tmp[i];
            //0   4  = 4
            //1   4 = 5
        }
    }

    public static void mergeSortInternal(int[] array,int left,int right) {
        if(left >= right) {
            return;
        }
        int mid = (left+right)/2;
        mergeSortInternal(array,left,mid);//递归实现
        mergeSortInternal(array,mid+1,right);
        merge(array,left,right,mid);
    }
    public static void main(String[] args) {
        int[] array = {2,4,1,7,5,8,9,10,3,6,0};
        mergeSortInternal(array,0,array.length-1);
        System.out.println(Arrays.toString(array));
    }
}
