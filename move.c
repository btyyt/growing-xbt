#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
void move(int a[], int sz)
{
	int n=0;
	int m = sz - 1;
	while (n < m)
	{
		while ((n<m)&&(a[n] % 2 == 1))
		{
			n++;
		}
		while ((n<m)&&(a[m] % 2 == 0))
		{
			m--;
		}
		if (n < m)
		{
			int t = a[n];
			a[n] = a[m];
			a[m] = t;
		}
	}
}
void print(int a[], int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", a[i]);
	}
}
int main()
{
	int a[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	int sz = sizeof(a) / sizeof(a[0]);
	move(a, sz);
	print(a, sz);
	return 0;
}